<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>推广数据报表</title>
	<meta name="keywords" content="<?php echo ($title); ?>" />
	<meta name="description" content="<?php echo ($title); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- basic styles -->
		<link href="/Public/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="/Public/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->

		<link rel="stylesheet" href="/Public/assets/css/jquery-ui-1.10.3.full.min.css" />

		<!-- fonts -->
		
		
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/assets/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="/Public/assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="/Public/assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="/Public/assets/js/html5shiv.js"></script>
		<script src="/Public/assets/js/respond.min.js"></script>
		<![endif]-->
</head>
<body>
	<div class="main-container" id="main-container">
		<div class="main-container-inner">
			<div class="page-content">
				<div class="page-header">
					<h1>
						<?php echo ($title); ?>
						<small>
							<i class="icon-double-angle-right"></i>
						</small>
					</h1>
					</div><!-- /.page-header -->	
  
			<div class="alert alert-block alert-success">
				<div style="padding-bottom:24px;">
				<form action="" method="get" name="reportFrom1">
					<div class="col-xs-12 col-sm-3">
						<label for="form-field-select-1">群号</label>	
						<input type="text" name="qun" value="<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):''); ?>" />
					</div>
					<div class="col-xs-12 col-sm-3">
						<label for="form-field-select-1">状态</label>
	
						<select class="form-control" id="form-field-select-1" name="payStatus">
							<option value="">全部</option>
							<option value="0" <?php if($_GET['payStatus']== '0'): ?>selected = true<?php endif; ?> >交易完成，未提现</option>
							<option value="1" <?php if($_GET['payStatus']== '1'): ?>selected = true<?php endif; ?> >交易完成，提现中</option>
							<option value="2" <?php if($_GET['payStatus']== '2'): ?>selected = true<?php endif; ?> >交易完成，已提现</option>
	
						</select>
					</div>
					
					
					<input  type="submit" class="btn btn-info btn-sm" value="查询">
				
					
				</form>
				</div>
			</div> 
  
  

		
				<div class="row">
					<div class="col-xs-12">
						<!-- PAGE CONTENT BEGINS -->

						<div class="row">


							<div class="modal-body no-padding">
								<table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
									<thead>
										<tr>
											<th>下单时间</th>
											<th>商品信息</th>
											<th>商品ID</th>

											<th>所属店铺	</th>
											<th>商品数</th>
											<th>商品单价</th>
											<th>订单状态</th>
											<th>分成比率</th>
											<th>付款金额</th>
											<th>结算金额</th>
											<th>结算时间</th>
											<th>佣金比率</th>
											<th>佣金金额</th>
											<th>订单编号</th>
											<th>广告位名称</th>
											<th>订单类型</th>
											<th>操作</th>
											<th>高级</th>
										</tr>
									</thead>

									<tbody>
									
<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$list): $mod = ($i % 2 );++$i;?><tr>
											<td>
												<?php echo ($list["clicktime"]); ?></td>
											<td><?php echo ($list["goodstitle"]); ?></td>
											<td><?php echo ($list["goodsid"]); ?></td>
											<td><?php echo ($list["shopname"]); ?></td>
											<td><?php echo ($list["goodsnums"]); ?></td>
											
											<td><?php echo ($list["goodsprice"]); ?></td>
											<td><?php echo ($list["orderstatus"]); ?></td>
											<td><?php echo ($list["splitratio"]); ?></td>
											<td><?php echo ($list["paymentamount"]); ?></td>
											<td><?php echo ($list["settlementamount"]); ?></td>
											<td><?php echo ($list["settlementtime"]); ?></td>
											<td><?php echo ($list["commissionrate"]); ?></td>
											<td><?php echo ($list["commissionamount"]); ?></td>
											<td><?php echo ($list["ordernumber"]); ?></td>
											<td><?php echo ($list["groupname"]); ?></td>
											<td><?php echo ($list["ordertype"]); ?></td>
											<td><?php echo ($list["zhifu"]); ?>
											<?php if($list["paybtn"] == '1'): ?><button onClick="payed('<?php echo ($list["goodsid"]); ?>','<?php echo ($list["ordernumber"]); ?>','<?php echo ($_POST['qun']); ?>');">确定支付提现</buttion>
											<?php else: endif; ?>	
											</td>
											<td>
												<button onClick="delPayInfo('<?php echo ($list["goodsid"]); ?>','<?php echo ($list["ordernumber"]); ?>','<?php echo ($_POST['qun']); ?>');">删除</buttion>
											</td>
										</tr><?php endforeach; endif; else: echo "" ;endif; ?>
										
									</tbody>
								</table>
							</div>

							<div class="modal-footer no-margin-top">

								<ul class="pagination pull-right no-margin">
									<li class="prev disabled">
									<?php echo ($page); ?>
									</li>
								</ul>
							</div>

								<div class="row">
									<div class="col-xs-12 col-sm-3 widget-container-span">
										<div class="widget-box">
											<div class="widget-header">
												<h5 class="smaller">交易进行中</h5>

												<div class="widget-toolbar">
													<span class="label label-success">
														<?php echo ($dealing["id"]); ?>单
														<i class="icon-arrow-up"></i>
													</span>
												</div>
											</div>

											<div class="widget-body">
												<div class="widget-main padding-6">
													<div class="alert alert-info"> 可自行计算 </div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3 widget-container-span">
										<div class="widget-box">
											<div class="widget-header">
												<h5 class="smaller">交易完成，未提现</h5>

												<div class="widget-toolbar">
													<span class="label label-success">
														<?php echo ($unpay["id"]); ?>单
														<i class="icon-arrow-up"></i>
													</span>
												</div>
											</div>

											<div class="widget-body">
												<div class="widget-main padding-6">
													<div class="alert alert-info"> <?php echo ($unpay["commissionamount"]); ?> ￥</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3 widget-container-span">
										<div class="widget-box">
											<div class="widget-header">
												<h5 class="smaller">交易完成，提现中</h5>

												<div class="widget-toolbar">
													<span class="label label-success">
														<?php echo ($paying["id"]); ?>单
														<i class="icon-arrow-up"></i>
													</span>
												</div>
											</div>

											<div class="widget-body">
												<div class="widget-main padding-6">
													<div class="alert alert-info"> <?php echo ($paying["commissionamount"]); ?> ￥</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3 widget-container-span">
										<div class="widget-box">
											<div class="widget-header">
												<h5 class="smaller">交易完成，已提现</h5>

												<div class="widget-toolbar">
													<span class="label label-success">
														<?php echo ($payed["id"]); ?>单
														<i class="icon-arrow-up"></i>
													</span>
												</div>
											</div>

											<div class="widget-body">
												<div class="widget-main padding-6">
													<div class="alert alert-info"> <?php echo ($payed["commissionamount"]); ?> ￥</div>
												</div>
											</div>
										</div>
									</div>
									
									
									
									
								</div>
							
							
							
						</div><!-- /row -->

						<div class="hr hr32 hr-dotted"></div>



						<!-- PAGE CONTENT ENDS -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>
			

		</div><!-- /.main-container-inner -->
	</div><!-- /.main-container -->

	
			<!-- basic scripts -->



		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='/Public/assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='/Public/assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='/Public/assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="/Public/assets/js/bootstrap.min.js"></script>
		<script src="/Public/assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="/Public/assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="/Public/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="/Public/assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="/Public/assets/js/jquery.slimscroll.min.js"></script>
		<script src="/Public/assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="/Public/assets/js/jquery.sparkline.min.js"></script>
		<script src="/Public/assets/js/flot/jquery.flot.min.js"></script>
		<script src="/Public/assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="/Public/assets/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="/Public/assets/js/ace-elements.min.js"></script>
		<script src="/Public/assets/js/ace.min.js"></script>

<script language="javascript" type="text/javascript">
	function payed(goodsid,ordernumber,qun)
	{
		 $.ajax(
			{
				type: 'post',
				url: '/index.php/Admins/Ajax/payEd',
				cache:false,
				dataType: 'json',
				data: {"goodsid": goodsid,"ordernumber":ordernumber,"qun":qun},
				async: false,
				beforeSend:function()
				{//触发ajax请求开始时执行
				},                
				success: function (msg, textStatus) 
				{//有数据返回时
				//alert(msg.info);
					if(msg.result==1)
					{
						alert(msg.info);
						window.location.reload();
					}
					else
					{
					   //$("#kouling"+msg.info).html("获取失败");              
					}
				},
				error:function (textStatus) 
				{//网络繁忙 错误             
				},
				complete: function(msg, textStatus)
				{ //ajax请求完成时执行                 
					if(msg.result==1)
					{
					}
				}                
			}); 
	}
	

	function delPayInfo(goodsid,ordernumber,qun)
	{
		
		var msg = "您真的确定要删除吗？\n\n请确认！";
		if (confirm(msg)==false){
			return false;
		}		
		 $.ajax(
			{
				type: 'post',
				url: '/index.php/Admins/Ajax/delPayInfo',
				cache:false,
				dataType: 'json',
				data: {"goodsid": goodsid,"ordernumber":ordernumber,"qun":qun},
				async: false,
				beforeSend:function()
				{//触发ajax请求开始时执行
				},                
				success: function (msg, textStatus) 
				{//有数据返回时
				//alert(msg.info);
					if(msg.result==1)
					{
						alert(msg.info);
						window.location.reload();
					}
					else
					{
					   //$("#kouling"+msg.info).html("获取失败");              
					}
				},
				error:function (textStatus) 
				{//网络繁忙 错误             
				},
				complete: function(msg, textStatus)
				{ //ajax请求完成时执行                 
					if(msg.result==1)
					{
					}
				}                
			}); 
	}
</script>
</body>
</html>