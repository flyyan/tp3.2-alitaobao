<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- basic styles -->
		<link href="/Public/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="/Public/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->

		<link rel="stylesheet" href="/Public/assets/css/jquery-ui-1.10.3.full.min.css" />

		<!-- fonts -->
		
		
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/assets/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="/Public/assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="/Public/assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="/Public/assets/js/html5shiv.js"></script>
		<script src="/Public/assets/js/respond.min.js"></script>
		<![endif]-->
<title>登录窗口</title>
</head>

<body>
<div class="row">
	<div class="col-xs-12">
<div class="position-relative">
	<div id="login-box" class="login-box visible widget-box no-border">
		<div class="widget-body">
			<div class="widget-main">
				<h4 class="header blue lighter bigger">
					<i class="icon-coffee green"></i>
					登录窗口</h4>

				<div class="space-6"></div>

				<form action="./Login" method="post" name="userForm">
					<fieldset>
						<label class="block clearfix">
							<span class="block input-icon input-icon-right">
								<input type="text" class="form-control" placeholder="Username" name="username" />
								<i class="icon-user"></i>														</span>													</label>

						<label class="block clearfix">
							<span class="block input-icon input-icon-right">
								<input type="password" class="form-control" placeholder="Password" name="password" />
								<i class="icon-lock"></i>														</span>													</label>

						<div class="space"></div>

						<label class="block clearfix">
							<span class="block input-icon input-icon-right">
								<input type="text" class="form-control" placeholder="验证码" name="verifyimg" />
						</label>
						<br/><br/><br/><br/>
						<label class="block clearfix">
							<img src="../../Sys/VerifyImg/" onclick="this.src='../../Sys/VerifyImg/'+'?'+Math.random()">
						</label>

						<div class="clearfix">
							<label class="inline">
								<input type="checkbox" class="ace" />
								<span class="lbl"> Remember Me</span>														</label>

							<input class="width-35 pull-right btn btn-sm btn-primary" type="submit" value="登录">
		
						</div>

						<div class="space-4"></div>
					</fieldset>
				</form>

				<div class="social-or-login center">
					<span class="bigger-110">Or Login Using</span>											</div>

				
			</div><!-- /widget-main -->

										
		</div><!-- /widget-body -->
	</div><!-- /login-box -->

</div>

</div>
</div>

		<!-- inline scripts related to this page -->

		<script type="text/javascript">
			function show_box(id) {
			 jQuery('.widget-box.visible').removeClass('visible');
			 jQuery('#'+id).addClass('visible');
			}
			if(top.location!==self.location){
				top.location.href=self.location.href; 
			} 
		</script>
</body>
</html>