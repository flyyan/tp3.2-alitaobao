<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>控制台 - 大淘客总库</title>
	<meta name="keywords" content="<?php echo ($title); ?>" />
	<meta name="description" content="<?php echo ($title); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- basic styles -->
		<link href="/Public/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="/Public/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->

		<link rel="stylesheet" href="/Public/assets/css/jquery-ui-1.10.3.full.min.css" />

		<!-- fonts -->
		
		
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/assets/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="/Public/assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="/Public/assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="/Public/assets/js/html5shiv.js"></script>
		<script src="/Public/assets/js/respond.min.js"></script>
		<![endif]-->
</head>
<body>
	<div class="main-container" id="main-container">
		<div class="main-container-inner">
			<div class="page-content">
		
				<div class="row">
					<div class="col-xs-12">
						<!-- PAGE CONTENT BEGINS -->

						<div class="row">
						
						<div class="widget-box">
							<div class="widget-header widget-header-small">
								<h5 class="lighter">搜索兼职</h5>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<form class="form-search">
										<div class="row">
											<div class="col-xs-12 col-sm-8">
												<div class="input-group">
													<input type="text" class="form-control search-query" placeholder="输入兼职名称">
													<span class="input-group-btn">
														<button type="button" class="btn btn-purple btn-sm">
															搜索
															<i class="icon-search icon-on-right bigger-110"></i>																		</button>
													</span>	
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="space-4"></div>

						<div class="modal-body no-padding">
							<table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
								<thead>
									<tr>
										<th>群号</th>
										<th>群名称</th>
										<th>群管理</th>
										<th>PID</th>										
										<th>联系QQ</th>
										<th>是否激活</th>

										<th>
											<i class="icon-time bigger-110"></i>
											操作	</th>
										<th>外推地址</th>
									</tr>
								</thead>

								<tbody>
									<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$list): $mod = ($i % 2 );++$i;?><tr>
										<td>
											<a href="#"><?php echo ($list["groupid"]); ?></a>															</td>
										<td><?php echo ($list["grouptitle"]); ?></td>
										<td><?php echo ($list["groupname"]); ?></td>
										<td><?php echo ($list["pid"]); ?></td>
										<td><?php echo ($list["groupqq"]); ?></td>
										<td><?php echo ($list["isaction"]); ?></td>
										<td>
										<a href="./EditPromoterForm/promoterId/<?php echo ($list["id"]); ?>">
										<button class="btn btn-xs btn-info">
											<i class="icon-edit bigger-120"></i>
										</button>
										</a>
										<a href="javascript:void(0);" class="red"  onClick="javascript:return p_del('<?php echo ($list["id"]); ?>')">
											<button class="btn btn-xs btn-danger">
											<i class="icon-trash bigger-120"></i>
										</button>
										</a>
										
										</td>
										<td>
										<a href="/index.php/Home/Product/getData/qun/<?php echo ($list["groupid"]); ?>" target="_blank">推广网址</a>
										</td>
									</tr><?php endforeach; endif; else: echo "" ;endif; ?>
									
								</tbody>
							</table>
						</div>

						<div class="modal-footer no-margin-top">

							<ul class="pagination pull-right no-margin">
							
								<li class="prev disabled">
								<?php echo ($page); ?>
								</li>
							</ul>
						</div>

							
						</div><!-- /row -->

						<div class="hr hr32 hr-dotted"></div>



						<!-- PAGE CONTENT ENDS -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>
			

		</div><!-- /.main-container-inner -->
	</div><!-- /.main-container -->

		<script type="text/javascript">			
			
			function p_del(promoterid) { 
			var msg = "您真的确定要删除吗？\n\n\n此步谨慎,会导致此推广者的信息删除！及群下所有产品删除"; 
				if (confirm(msg)==true){ 
					 window.location.href="./delPromoter/?id="+promoterid;
					return false; 
				}else{ 
					return false; 
				} 
			} 			
		</script>
	
			<!-- basic scripts -->



		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='/Public/assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='/Public/assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='/Public/assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="/Public/assets/js/bootstrap.min.js"></script>
		<script src="/Public/assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="/Public/assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="/Public/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="/Public/assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="/Public/assets/js/jquery.slimscroll.min.js"></script>
		<script src="/Public/assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="/Public/assets/js/jquery.sparkline.min.js"></script>
		<script src="/Public/assets/js/flot/jquery.flot.min.js"></script>
		<script src="/Public/assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="/Public/assets/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="/Public/assets/js/ace-elements.min.js"></script>
		<script src="/Public/assets/js/ace.min.js"></script>

</body>
</html>