<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>控制台 - 推广管理系统</title>
		<meta name="keywords" content="推广管理系统" />
		<meta name="description" content="推广管理系统" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<link href="/Public/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="/Public/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->

		<link rel="stylesheet" href="/Public/assets/css/jquery-ui-1.10.3.full.min.css" />

		<!-- fonts -->
		
		
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/assets/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="/Public/assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="/Public/assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="/Public/assets/js/html5shiv.js"></script>
		<script src="/Public/assets/js/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript" src="/Public/js/iframeHeight.js"></script>
	</head>

	<body>
		<div class="navbar navbar-default" id="navbar">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="#" class="navbar-brand">
						<small>
							<i class="icon-leaf"></i>
							推广管理系统
						</small>
					</a><!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="/Public/assets/avatars/user.jpg" alt="Jason's Photo" />
								<span class="user-info">
									<small>欢迎光临,</small>
									<?php echo (cookie('username')); ?>
								</span>

								<i class="icon-caret-down"></i>
							</a>

							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="#">
										<i class="icon-cog"></i>
										设置
									</a>
								</li>

								<li>
									<a href="#">
										<i class="icon-user"></i>
										个人资料
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="./Sys/Loginout/">
										<i class="icon-off"></i>
										退出
									</a>
								</li>
							</ul>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
</div>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<div class="sidebar" id="sidebar">
						<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
	</script>
	<div class="sidebar-shortcuts" id="sidebar-shortcuts">
			<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
							<button class="btn btn-success">
								<i class="icon-signal"></i>
							</button>

							<button class="btn btn-info">
								<i class="icon-pencil"></i>
							</button>

							<button class="btn btn-warning">
								<i class="icon-group"></i>
							</button>

							<button class="btn btn-danger">
								<i class="icon-cogs"></i>
							</button>
				</div>

			<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
							<span class="btn btn-success"></span>

							<span class="btn btn-info"></span>

							<span class="btn btn-warning"></span>

							<span class="btn btn-danger"></span>
			</div>
	</div><!-- #sidebar-shortcuts -->

<ul class="nav nav-list">
	<li class="active">
		<a href="index.html">
			<i class="icon-dashboard"></i>
			<span class="menu-text"> 控制台 </span>
		</a>
	</li>

	

	<li>
		<a href="#" class="dropdown-toggle">
			<i class="icon-desktop"></i>
			<span class="menu-text"> 兼职管理 </span>

			<b class="arrow icon-angle-down"></b>
		</a>

		<ul class="submenu">
			<li>
				<a href="./Promoter/PromoterLists"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					兼职列表
				</a>
			</li>

			<li>
				<a href="./Promoter/AddPromoterForm"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					新增兼职
				</a>
			</li>

		</ul>
	</li>

	<li>
		<a href="#" class="dropdown-toggle">
			<i class="icon-list"></i>
			<span class="menu-text"> 大淘客总库 </span>

			<b class="arrow icon-angle-down"></b>
		</a>

		<ul class="submenu">
			<li>
				<a href="./DataokeApi/lists" target="iframepage">
					<i class="icon-double-angle-right"></i>
					产品列表
				</a>
			</li>

			<li>
				<a href="./DataokeApi/productSyn"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					产品同步
				</a>
			</li>		
			<li>
				<a href="./DataokeApi/daterepairView"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					产品维护
				</a>
			</li>

			<li>
				<a href="./DataokeApi/quanCheck"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					无券检测
				</a>
			</li>			
		</ul>
	</li>

	<li>
		<a href="#" class="dropdown-toggle">
		<i class="icon-edit"></i>
		<span class="menu-text">阿里总库</span>
		<b class="arrow icon-angle-down"></b></a>
	
		<ul class="submenu">
			<li>
				<a href="form-elements.html"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					产品列表(待开发)	</a>
			</li>	
			<li>
				<a href="wysiwyg.html"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					产品同步(待开发)
				</a>
			</li>
	
			<li>
				<a href="dropzone.html"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					产品维护(待开发)
				</a>
			</li>
		</ul>
	</li>
	
	<li>
		<a href="#" class="dropdown-toggle">
		<i class="icon-edit"></i>
		<span class="menu-text">推广产品管理</span>
		<b class="arrow icon-angle-down"></b></a>
				
		<ul class="submenu">
			<li>
				<a href="./Promoter/oneUpload"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					逐一上传(待开发)
				</a>
			</li>

			<li>
				<a href="./Promoter/moreUpload"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					批量上传
				</a>
			</li>
			
			<li>
				<a href="./Promoter/dataSyn"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					数据维护
				</a>
			</li>
			
		</ul>
		
	</li>

	<li>
			<a href="#" class="dropdown-toggle">
			<i class="icon-edit"></i>
			<span class="menu-text">助手相关(测试版)</span>
			<b class="arrow icon-angle-down"></b></a>
					
			<ul class="submenu">
				<li>
					<a href="#"  target="iframepage">
						<i class="icon-double-angle-right"></i>
						新建标示(待开发)
					</a>
				</li>
	
				<li>
					<a href="./DataokeApi/zhushouApiList"  target="iframepage">
						<i class="icon-double-angle-right"></i>
						获取助手转连结果
					</a>
				</li>
			</ul>
			
		</li>

	
	<li>
		<a href="#" class="dropdown-toggle">
			<i class="icon-tag"></i>
			<span class="menu-text"> 系统设置 </span>

			<b class="arrow icon-angle-down"></b>
		</a>

		<ul class="submenu">
			<li>
				<a href="profile.html"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					用户信息(待开发)
				</a>
			</li>

			<li>
				<a href="login.html"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					密码修改(待开发)
				</a>
			</li>
			
			<li>
				<a href="./Reportform/Uploads"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					报表上传
				</a>
			</li>
			
			<li>
				<a href="../Admins/Reportform/getListsInfo"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					报表查看
				</a>
			</li>
			<li>
				<a href="../Admins/Reportform/DelReportform"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					报表删除
				</a>
			</li>			
		</ul>
	</li>

	<li>
		<a href="#" class="dropdown-toggle">
			<i class="icon-file-alt"></i>

			<span class="menu-text">
				帮助文档
				<span class="badge badge-primary ">5</span>
			</span>

			<b class="arrow icon-angle-down"></b>
		</a>

		<ul class="submenu">
			<li>
				<a href="faq.html"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					帮助
				</a>
			</li>
			<li>
				<a href="./Promoter/lists"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					推广网址
				</a>
			</li>
			<li>
				<a href="../Home/Product/getData/qun/1"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					1群推广网址对外
				</a>
			</li>
			<li>
				<a href="../Home/Product/getAllData/qun/1"  target="iframepage">
					<i class="icon-double-angle-right"></i>
					1群推广总产品库
				</a>
			</li>
		</ul>
	</li>
</ul><!-- /.nav-list -->

	<div class="sidebar-collapse" id="sidebar-collapse">
		<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
	</div>

	<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
	</script>
				</div>

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="#">首页</a>
							</li>
							<li class="active">控制台</li>
						</ul><!-- .breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- #nav-search -->
					</div>
					<iframe src='./index/welcome' id='iframepage' name='iframepage' frameBorder='0' scrolling=no width='75%' onLoad='Javascript:dyniframesize("iframepage")'></iframe>
					<!-- /.page-content -->
				</div><!-- /.main-content -->

	
			</div><!-- /.main-container-inner -->


		</div><!-- /.main-container -->




		
			<!-- basic scripts -->



		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='/Public/assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='/Public/assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='/Public/assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="/Public/assets/js/bootstrap.min.js"></script>
		<script src="/Public/assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="/Public/assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="/Public/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="/Public/assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="/Public/assets/js/jquery.slimscroll.min.js"></script>
		<script src="/Public/assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="/Public/assets/js/jquery.sparkline.min.js"></script>
		<script src="/Public/assets/js/flot/jquery.flot.min.js"></script>
		<script src="/Public/assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="/Public/assets/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="/Public/assets/js/ace-elements.min.js"></script>
		<script src="/Public/assets/js/ace.min.js"></script>

</body>
</html>