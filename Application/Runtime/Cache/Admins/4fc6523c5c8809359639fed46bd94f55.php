<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>控制台 - 大淘客总库</title>
	<meta name="keywords" content="<?php echo ($title); ?>" />
	<meta name="description" content="<?php echo ($title); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- basic styles -->
		<link href="/Public/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="/Public/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->

		<link rel="stylesheet" href="/Public/assets/css/jquery-ui-1.10.3.full.min.css" />

		<!-- fonts -->
		
		
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/assets/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="/Public/assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="/Public/assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="/Public/assets/js/html5shiv.js"></script>
		<script src="/Public/assets/js/respond.min.js"></script>
		<![endif]-->
</head>
<body>
	<div class="main-container" id="main-container">
		<div class="main-container-inner">
			<div class="page-content">
				<div class="page-header">
					<h1>
						<?php echo ($title); ?>
						<small>
							<i class="icon-double-angle-right"></i>
							 查看
						</small>
					</h1>
					</div><!-- /.page-header -->
		
				<div class="row">
					<div class="col-xs-12">
						<!-- PAGE CONTENT BEGINS -->
						


					<form class="form-horizontal" role="form" name="promoter" action="./AddPromoter" method="post">
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 兼职群号： </label>
					
								<div class="col-sm-9">
									<input type="text" id="groupId" name="groupId" placeholder="兼职群号" class="col-xs-10 col-sm-5" />
								</div>
							</div>
					
							<div class="space-4"></div>
					
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 群名称： </label>
					
								<div class="col-sm-9">
									<input type="text" id="groupTitle" name = "groupTitle" placeholder="群名称" class="col-xs-10 col-sm-5" />
								</div>
							</div>
					
							<div class="space-4"></div>

							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> PID： </label>
					
								<div class="col-sm-9">
									<input type="text" id="PID" name = "PID" placeholder="PID" class="col-xs-10 col-sm-5" />
								</div>
							</div>
					
							<div class="space-4"></div>
					
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 管理姓名： </label>
					
								<div class="col-sm-9">
									<input type="text" id="groupName" name = "groupName" placeholder="管理姓名" class="col-xs-10 col-sm-5" />
								</div>
							</div>
					
							<div class="space-4"></div>
					
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 联系方式： </label>
					
								<div class="col-sm-9">
									<input type="text" id="groupQQ" name ="groupQQ" placeholder="联系方式" class="col-xs-10 col-sm-5" />
								</div>
							</div>
					
							<div class="space-4"></div>
					
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 是否激活： </label>
								<div class="col-sm-9">
									<label>
										<input id="isAction" name="isAction" class="ace ace-switch ace-switch-7" type="checkbox">
										<span class="lbl"></span>
									</label>
								</div>
							</div>
					
							<div class="space-4"></div>
					
							
							<div class="clearfix form-actions">
								<div class="col-md-offset-3 col-md-9">
									<button class="btn btn-info" type="submit">
										<i class="icon-ok bigger-110"></i>
										Submit											</button>
					
									     
									<button class="btn" type="reset">
										<i class="icon-undo bigger-110"></i>
										Reset											</button>
								</div>
							</div>
					
							<div class="hr hr-24"></div>
														
														
					</form>







						<div class="hr hr32 hr-dotted"></div>
						<!-- PAGE CONTENT ENDS -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>
			

		</div><!-- /.main-container-inner -->
	</div><!-- /.main-container -->


			<!-- basic scripts -->



		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='/Public/assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='/Public/assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='/Public/assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="/Public/assets/js/bootstrap.min.js"></script>
		<script src="/Public/assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="/Public/assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="/Public/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="/Public/assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="/Public/assets/js/jquery.slimscroll.min.js"></script>
		<script src="/Public/assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="/Public/assets/js/jquery.sparkline.min.js"></script>
		<script src="/Public/assets/js/flot/jquery.flot.min.js"></script>
		<script src="/Public/assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="/Public/assets/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="/Public/assets/js/ace-elements.min.js"></script>
		<script src="/Public/assets/js/ace.min.js"></script>

</body>
</html>