<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>推广数据报表</title>
	<meta name="keywords" content="<?php echo ($title); ?>" />
	<meta name="description" content="<?php echo ($title); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
			<link rel="stylesheet" href="/Public/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css" />
		<script src="/Public/js/jquery-1.11.1.min.js"></script>
		<script src="/Public/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>

<!-- basic styles -->
		<link href="/Public/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="/Public/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->


		<!-- fonts -->
	
		
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/assets/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="/Public/assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="/Public/assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="/Public/assets/js/html5shiv.js"></script>
		<script src="/Public/assets/js/respond.min.js"></script>
		<![endif]-->
	<style type="text/css">
	img.wp-smiley,
	img.emoji {
		display: inline !important;
		border: none !important;
		box-shadow: none !important;
		height: 1em !important;
		width: 1em !important;
		margin: 0 .07em !important;
		vertical-align: -0.1em !important;
		background: none !important;
		padding: 0 !important;
	}
	</style>
	<link rel="stylesheet" id="wptao-style-css" href="/Public/home/getdata/style.css" type="text/css" media="all">
	
	<script language="javascript" src="/Public/js/jquery-1.11.1.min.js"></script>
	<script language="javascript" src="/Public/assets/js/date-time/bootstrap-datepicker.min.js"></script>	
	<script src="/Public/assets/js/date-time/bootstrap-datepicker.zh-CN.js"></script>
	<link rel="stylesheet" href="/Public/assets/css/datepicker.css" />
	
	<script src="/Public/js/main.js"></script>	
</head>
<body>
<!-- 网站顶部 -->
<div class="top_sitebar">
  <div class="grid_auto sitebar_container cf">
    <div class="sitebar_entry fl">
	  <div class="login_before">
			  </div>
	</div>
    <div class="sitebar_topnav fr">
	    </div>
  </div>
</div>
<!-- 网站顶部 end -->
<!-- 头部 -->
<div class="header">
  <div class="grid_auto header_container cf"> 
  <a class="site_logo fl" href="<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>" title="优惠券直播_商家内部优惠券"></a>
    <div class="protection fl">
      <ul>
        <li><i class="ico_site ico_pro_1"></i><span>职业买手爆料</span></li>
        <li><i class="ico_site ico_pro_2"></i><span>中立态度甄选</span></li>
        <li><i class="ico_site ico_pro_3"></i><span>人工智能推荐</span></li>
      </ul>
    </div>
    <div class="search_area fr">
      <div class="search_box cf">
       <form action=""  method="get" name="productForm">
          <span class="search_input fl">		  
          <input id="keywords" class="keywords txt_focus" name="keywords" placeholder="请输入想找的宝贝" type="text">
          </span>
          <input class="search_submit fl" value="" type="submit">
       </form>
      </div>
    </div>
  </div>

	  <!-- 导航 -->
	  <div class="header_nav" style="">
		  <div class="menu-nav-container">
			<ul id="menu-HOME" class="menu">
		<!--li class="current-menu-index"><a href="/index.php/Home/Reportform/getList/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>">首页</a></li-->
		<li class="current-menu-9"><a href="/index.php/Home/Product/getAllData/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>">你的总库</a></li>
		<li class="current-menu-20"><a href="/index.php/Home/Reportform/getList/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>">报表</a></li>	
		<li class="current-menu-qun"><a href="#"></a></li>
		<li class="current-menu-qun"><a href="#"></a></li>
	
		
			</ul>
			<a class="publish_btn islogin" target="_blank" href="#"><i></i>爱分享</a>	  </div>
		  </div>
	  <!-- 导航 end -->
	  
	</div>
	<!-- 头部 end -->
	
	<div id="header-mobile" class="mobile-header">
		<div class="header-container-wrapper">
		<div class="container header-mobile-wrapper">
		  <div class="header-mobile-inner">
			<div class="toggle-icon-wrapper toggle-mobile-menu" data-ref="nav-menu-mobile" data-drop-type="fly">
			  <div class="toggle-icon"> <span></span></div>
			</div>
			<div class="header-middle">
			  <a href="#"><i class="small-logo"></i> 优惠券</a> <span style="float:right">点击右上角在浏览器中打开↑</span>
			</div>
			<div class="header-customize">
			  <div class="header-customize-item"> </div>
			</div>
		  </div>
		  <div id="nav-menu-mobile" class="header-mobile-nav">
			<form class="search-form-menu-mobile" action="" method="get" name="productForm">
			  <input name="keywords" placeholder="请输入想找的宝贝" type="text">
			  <button type="submit"><i class="fa fa-search"></i></button>
			</form>
			<ul class="nav-menu-mobile">
		<!--li class="current-menu-index"><a href="/index.php/Home/Reportform/getList/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>">首页</a></li-->
		<li class="current-menu-9"><a href="/index.php/Home/Reportform/getAllData/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>">你的总库</a></li>
		<li class="current-menu-20"><a href="/index.php/Home/Reportform/getList/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>">报表</a></li>	
		<li class="current-menu-qun"><a href="#"></a></li>
		<li class="current-menu-qun"><a href="#"></a></li>
	
		
			</ul>
			<div class="main-menu-bottom"></div>
		  </div>
		  <div class="main-menu-overlay"></div>
		</div>
	  </div>
	</div>
	<!-- /页头 -->


	<div class="main-container" id="main-container">
		<div class="main-container-inner">
			<div class="page-content">
				<div class="page-header">
					<h1>
						<?php echo ($title); ?>
						<small>
							<i class="icon-double-angle-right"></i>
							 查看--订单最后更新时间：<?php echo ($uptime); ?>【<font color="red">系统正常情况下5-10分钟自动更新一次</font>】
						</small>
					</h1>
					</div><!-- /.page-header -->	

	  <script>
	  $(document).ready(function(){
			$( "#datepicker_start" ).datepicker({
				format: 'yyyy-mm-dd',
				showOtherMonths: true,
				selectOtherMonths: false,
				//isRTL:true,
				language: 'zh-CN',
				autoclose: true,
				todayHighlight: true
			});
				$( "#datepicker_end" ).datepicker({
				 format: 'yyyy-mm-dd',
				showOtherMonths: true,
				selectOtherMonths: false,
				//isRTL:true,
				language: 'zh-CN',
				autoclose: true,
				todayHighlight: true
			});
 		});

  </script>
  
			<div class="alert alert-block alert-success">
				<div style="padding-bottom:24px;">
				<form action="" method="get" name="reportFrom1">
				  	<div class="col-sm-9">
						<label> 下单时间</label>
						<span class="input-icon input-icon-right">
							<input type="text" name="startTime" id="datepicker_start" />
							<i class="icon-leaf blue"></i>
						</span>
	
						<span class="input-icon input-icon-right">
							<input type="text"  name="endTime" id="datepicker_end" />
							<i class="icon-leaf green"></i>
						</span>
					</div>
					<input type="hidden" name="qun" value="<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'12'); ?>" />
					<input  type="submit" class="btn btn-info btn-sm" value="查询">
				
					
				</form>
				</div>
			</div> 
  
  

		
				<div class="row">
					<div class="col-xs-12">
						<!-- PAGE CONTENT BEGINS -->

						<div class="row">


							<div class="modal-body no-padding">
								<table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
									<thead>
										<tr>
											<th>下单时间</th>
											<th style="width:200px;">商品信息</th>
											<!--th>商品ID</th-->

											<th>所属店铺	</th>
											<th>商品数</th>
											<!--th>商品单价</th-->
											<th>订单状态</th>
											<!--th>分成比率</th-->
											<th>付款金额</th>
											<!--th>结算金额</th-->
											<th>结算时间</th>
											<th>佣金比率</th>
											<th>佣金金额</th>
											<th>订单编号</th>
											<th>广告位名称</th>
											<th>订单类型</th>
											<th>操作</th>
										</tr>
									</thead>

									<tbody>
									
<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$list): $mod = ($i % 2 );++$i;?><tr>
											<td>
												<?php echo ($list["clicktime"]); ?></td>
											<td><?php echo ($list["goodstitle"]); ?></td>
											<!--td><?php echo ($list["goodsid"]); ?></td-->
											<td><?php echo ($list["shopname"]); ?></td>
											<td><?php echo ($list["goodsnums"]); ?></td>
											
											<!--td><?php echo ($list["goodsprice"]); ?></td-->
											<td>
												<?php if($list["paybtn"] == '1'): ?><b style="color:#FF0000;"><?php echo ($list["orderstatus"]); ?></b>
												<?php else: ?>
													<?php echo ($list["orderstatus"]); endif; ?>												
											</td>
											<!--td><?php echo ($list["splitratio"]); ?></td-->
											<td><?php echo ($list["paymentamount"]); ?></td>
											<!--td><?php echo ($list["settlementamount"]); ?></td-->
											<td><?php echo ($list["settlementtime"]); ?></td>
											<td><?php echo ($list["commissionrate"]); ?></td>
											<td><?php echo ($list["commissionamount"]); ?></td>
											<td><?php echo ($list["ordernumber"]); ?></td>
											<td><?php echo ($list["groupname"]); ?></td>
											<td><?php echo ($list["ordertype"]); ?></td>
											<td><?php echo ($list["zhifu"]); ?>											
											<?php if($list["paybtn"] == '1'): ?><button onClick="apply_pay('<?php echo ($list["goodsid"]); ?>','<?php echo ($list["ordernumber"]); ?>','<?php echo ($_GET['qun']); ?>');">申请提现</buttion>
											<?php else: endif; ?>											
											</td>
										</tr><?php endforeach; endif; else: echo "" ;endif; ?>
										
									</tbody>
								</table>
							</div>

							<div class="modal-footer no-margin-top">

								<ul class="pagination pull-right no-margin">
									<li class="prev disabled">
									<?php echo ($page); ?>
									</li>
								</ul>
							</div>

								<div class="row">
									<div class="col-xs-12 col-sm-3 widget-container-span">
										<div class="widget-box">
											<div class="widget-header">
												<h5 class="smaller">交易进行中</h5>

												<div class="widget-toolbar">
													<span class="label label-success">
														<?php echo ($dealing["id"]); ?>单
														<i class="icon-arrow-up"></i>
													</span>
												</div>
											</div>

											<div class="widget-body">
												<div class="widget-main padding-6">
													<div class="alert alert-info"> 可自行计算 </div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3 widget-container-span">
										<div class="widget-box">
											<div class="widget-header">
												<h5 class="smaller">交易完成，未提现</h5>

												<div class="widget-toolbar">
													<span class="label label-success">
														<?php echo ($unpay["id"]); ?>单
														<i class="icon-arrow-up"></i>
													</span>
												</div>
											</div>

											<div class="widget-body">
												<div class="widget-main padding-6">
													<div class="alert alert-info"> <?php echo ($unpay["commissionamount"]); ?> ￥</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3 widget-container-span">
										<div class="widget-box">
											<div class="widget-header">
												<h5 class="smaller">交易完成，提现中</h5>

												<div class="widget-toolbar">
													<span class="label label-success">
														<?php echo ($paying["id"]); ?>单
														<i class="icon-arrow-up"></i>
													</span>
												</div>
											</div>

											<div class="widget-body">
												<div class="widget-main padding-6">
													<div class="alert alert-info"> <?php echo ($paying["commissionamount"]); ?> ￥</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3 widget-container-span">
										<div class="widget-box">
											<div class="widget-header">
												<h5 class="smaller">交易完成，已提现</h5>

												<div class="widget-toolbar">
													<span class="label label-success">
														<?php echo ($payed["id"]); ?>单
														<i class="icon-arrow-up"></i>
													</span>
												</div>
											</div>

											<div class="widget-body">
												<div class="widget-main padding-6">
													<div class="alert alert-info"> <?php echo ($payed["commissionamount"]); ?> ￥</div>
												</div>
											</div>
										</div>
									</div>
									
									
									
									
								</div>
							
							
							
						</div><!-- /row -->

						<div class="hr hr32 hr-dotted"></div>



						<!-- PAGE CONTENT ENDS -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>
			

		</div><!-- /.main-container-inner -->
	</div><!-- /.main-container -->

<!-- topnav -->
<div class="top_nav" id="fixed_topnav" style="top: 0px;">

  <div class="grid_auto top_nav_cot cf"> <a href="/index.php/Home/Product/getAllData/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>" class="nav_logo_ico">优惠券直播</a>
	<ul class="menu">
    <!--li class="current-menu-index"><a href="/index.php/Home/Reportform/getList/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>">首页</a></li-->
	<li class="current-menu-9"><a href="/index.php/Home/Product/getAllData/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>">你的总库</a></li>
	<li class="current-menu-20"><a href="/index.php/Home/Reportform/getList/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>">报表</a></li>	
	<li class="current-menu-qun"><a href="#"></a></li>
	<li class="current-menu-qun"><a href="#"></a></li>

	
		</ul>
    <div class="top_nav_search fr">
      <form action="" method="get" name="productForm">
        <input id="nav_keywords" class="nav_keywords txt_focus" name="keywords" placeholder="请输入想找的宝贝" autocomplete="off" type="text">
        <input class="nav_search_btn" value="" type="submit">
      </form>
    </div>
  </div>
</div>
<!-- topnav end-->
	<!-- 页脚 -->
<div class="footer">
  <div class="grid_auto footer_container">
    <div class="site_copy">
      <p class="copyright">免费优惠券！比直接购买更省钱！每天更新淘宝天猫优惠产品信息/女装/护肤品及日用品，总有你需要的！ <br> </p>
      <div class="you_lian"> 
</div>
    </div>
  </div>
</div>



<div class="footer-mobile-wrapper">
	<a class="i-home" href="/index.php/Home/Reportform/getData/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>"><i></i><span>首页</span></a>
	<a class="i-9" href="/index.php/Home/Reportform/getAllData/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>"><i></i><span>你的总库</span></a>
	<a class="i-20" data-ref="nav-menu-mobile" data-drop-type="fly" href="/index.php/Home/Reportform/getList/qun/<?php echo ((isset($_GET['qun']) && ($_GET['qun'] !== ""))?($_GET['qun']):'2'); ?>"><i></i><span>报表</span></a>
	<a class="i-0" href="#"><i></i><span></span></a>
	<a class="i-qun" href="#"><i></i><span></span></a>
    <div class="top_nav_search fr">
      <form action="" method="get" name="productForm1">
        <input id="nav_keywords" class="nav_keywords txt_focus" name="keywords" placeholder="请输入想找的宝贝" autocomplete="off" type="text">
        <input class="nav_search_btn" value="" type="submit">
      </form>
    </div> 
</div>

<div id="layer_box" style="display: block;"><a class="gotop_btn" title="回到顶部" href="javascript:void(0);"><span>回到顶部</span></a></div>
<div class="hide">
 <script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1260758763'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s4.cnzz.com/stat.php%3Fid%3D1260758763' type='text/javascript'%3E%3C/script%3E"));</script>
</div>

			<!-- basic scripts -->

		<script src="/Public/assets/js/bootstrap.min.js"></script>
		<script src="/Public/assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!-- ace scripts -->

		<script src="/Public/assets/js/ace-elements.min.js"></script>
		<script src="/Public/assets/js/ace.min.js"></script>

<script language="javascript" type="text/javascript">
	function apply_pay(goodsid,ordernumber,qun)
	{
		 $.ajax(
			{
				type: 'post',
				url: '/index.php/Home/Ajax/applyPay',
				cache:false,
				dataType: 'json',
				data: {"goodsid": goodsid,"ordernumber":ordernumber,"qun":qun},
				async: false,
				beforeSend:function()
				{//触发ajax请求开始时执行
				},                
				success: function (msg, textStatus) 
				{//有数据返回时
				//alert(msg.info);
					if(msg.result==1)
					{
						alert(msg.info);
						window.location.reload();
					}
					else
					{
					   //$("#kouling"+msg.info).html("获取失败");              
					}
				},
				error:function (textStatus) 
				{//网络繁忙 错误             
				},
				complete: function(msg, textStatus)
				{ //ajax请求完成时执行                 
					if(msg.result==1)
					{
					}
				}                
			}); 
	}
</script>
</body>
</html>