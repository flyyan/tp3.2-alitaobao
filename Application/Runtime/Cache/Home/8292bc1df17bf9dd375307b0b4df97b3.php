<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>剁手复制</title>
			<link rel="stylesheet" href="/Public/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css" />
		<script src="/Public/js/jquery-1.11.1.min.js"></script>
		<script src="/Public/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>

<!-- basic styles -->
		<link href="/Public/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="/Public/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->


		<!-- fonts -->
	
		
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/assets/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="/Public/assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="/Public/assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="/Public/assets/js/html5shiv.js"></script>
		<script src="/Public/assets/js/respond.min.js"></script>
		<![endif]-->
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">

</head>
<body>
<div class="main-container">
	<div class="main-content">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="login-container">
					<div class="center">
						<h4 class="blue">
							<img src="/Public/images/app@2x.png" class="img-circle">		
						</h4>
					</div>

					<div class="space-6"></div>

					<div class="position-relative">
						<div id="login-box" class="login-box visible widget-box no-border">
							<div class="widget-body">
								<div class="widget-main">
									<h4 class="header blue lighter bigger" style="text-align:center;">
										<div id="kouling">
											<p><h1><?php echo ($kl); ?></h1></p>
										</div>
									</h4>
								<div style="text-align:center;">
									<p><button class="btn" data-clipboard-action="copy" data-clipboard-target="#kouling">
									点击复制</button></p>
								
								</div>


							<div class="social-or-login center">
								<span>点击按钮或长按全选复制上方淘口令<br/>打开「手机淘宝」即可「领取优惠券」并购买</span>
							</div><!-- /widget-main -->



									
								</div><!-- /widget-main -->
								
							</div><!-- /widget-body -->
						</div><!-- /login-box -->

					</div><!-- /position-relative -->
				</div>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>

<div class="bdsharebuttonbox">
	<a href="#" class="bds_more" data-cmd="more">分享到：</a>
	<a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博">新浪微博</a>
	<a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间">QQ空间</a>
	<a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信">微信</a>
	<a href="#" class="bds_douban" data-cmd="douban" title="分享到豆瓣网">豆瓣网</a>
	<a href="#" class="bds_tieba" data-cmd="tieba" title="分享到百度贴吧">百度贴吧</a>
	<a href="#" class="bds_mogujie" data-cmd="mogujie" title="分享到蘑菇街">蘑菇街</a>
	<a href="#" class="bds_isohu" data-cmd="isohu" title="分享到我的搜狐">我的搜狐</a>
</div>
<script>
	window._bd_share_config={"common":{"bdSnsKey":{"tsina":"376485574"},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"1","bdSize":"16"},"share":{"bdSize":16}};
	with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];
</script>
</body>

    <!-- 1. Define some markup -->
	<!--div id="logo1"><img src = 'http://f.51240.com/img/logo.gif?v=16091404' /></div>
    

    <!-- 2. Include library -->
    <script src="/Public/js/Clipboard/clipboard.min.js"></script>

    <!-- 3. Instantiate clipboard -->
    <script>
    var clipboard = new Clipboard('.btn');

    clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
    </script>


</html>