<?php
namespace Home\Model;
use Think\Model;
class ProductModel extends Model {
	protected $tableName = 'promoter_dataoke_product';


	public function getList($qun='1',$keywords = "")
	{
		$where = " B.groupId='$qun' and A.IsQunNull<>'1' ";
		$orderBy = ' ORDER BY B.time DESC ';
		if($keywords<>"")
		{
			$where .= " and A.Title Like '%".$keywords."%' ";
		}

		import('ORG.Util.Page');// 导入分页类
		
		$sql1 = "SELECT A.id
FROM  `".C('DB_PREFIX')."dataoke_product` AS A
INNER JOIN  `".C('DB_PREFIX')."promoter_dataoke_product` AS B ON A.GoodsID = B.goodsId
WHERE " .$where;
		$count = $this->execute($sql1);

		$Page       = new \Think\Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数
		//分页跳转的时候保证查询条件
		foreach($map as $key=>$val) {
			//$Page->parameter   .=   "$key=".urlencode($val).'&';
		}
		
		$sql = "SELECT A.GoodsID, A.Title, A.D_title, A.pic, A.Org_Price, A.Price, A.Commission, A.Introduce, A.Quan_price, A.Quan_time, A.Quan_condition, A.Quan_link, B.taokeShortUrl, B.quanUrl,B.fansbuyUrl,B.fansbuyShortUrl,B.fansKongling 
FROM  `".C('DB_PREFIX')."dataoke_product` AS A
INNER JOIN  `".C('DB_PREFIX')."promoter_dataoke_product` AS B ON A.GoodsID = B.goodsId
WHERE ".$where.$orderBy." 
LIMIT ".$Page->firstRow.','.$Page->listRows;
		//echo $sql;
//INNER JOIN 		
		$page       = $Page->show();// 分页显示输出
		$list = $this->query($sql);
		$result['list'] = $list ;
		$result['page'] = $page; 
		return $result;		
		
	}

	
	public function getImages($goodsid)
	{
		//$sql = "SELECT Pic FROM  `".C('DB_PREFIX')."dataoke_product` WHERE GoodsID='".$goodsid."'";
		$res = $this->table(C('DB_PREFIX').'dataoke_product')->where('GoodsID='.$goodsid)->getField('Pic');
		
		return $res;
		
		
	
	}
	public function getAllData($keywords = "",$cid = "0")
	{
		$where = "1 and A.IsQunNull<>'1' ";
		$orderBy = ' ORDER BY A.Ctime DESC ';
		if($keywords<>"")
		{
			$where .= " and A.Title Like '%".$keywords."%' ";
		}
		if($cid<>"0")
		{
			$where .= " and A.Cid = '".$cid."' ";
		}

		import('ORG.Util.Page');// 导入分页类
		
		$sql1 = "SELECT A.id
FROM  `".C('DB_PREFIX')."dataoke_product` AS A
WHERE " .$where;
		$count = $this->execute($sql1);

		$Page       = new \Think\Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数
		//分页跳转的时候保证查询条件
		foreach($map as $key=>$val) {
			//$Page->parameter   .=   "$key=".urlencode($val).'&';
		}
		$sql = "SELECT A.GoodsID, A.Title, A.D_title, A.pic, A.Org_Price, A.Price, A.Commission, A.Introduce,A.Quan_id, A.Quan_price, A.Quan_time, A.Quan_condition, A.Quan_link
FROM  `".C('DB_PREFIX')."dataoke_product` AS A
WHERE ".$where.$orderBy." 
LIMIT ".$Page->firstRow.','.$Page->listRows;
		//echo $sql;
//INNER JOIN 		
		$page       = $Page->show();// 分页显示输出
		$list = $this->query($sql);
		$result['list'] = $list ;
		$result['page'] = $page; 
		return $result;		
			
	}
	public function getFans($qun,$goodsid)
	{
		$sql = "select goodsId,quanId,sclick,fansKongling from ".C('DB_PREFIX')."fansbuy_sclick where groupId = '".$qun."' and goodsId = '".$goodsid."'";
		$data = $this->query($sql);	
		//$data = $this->execute($sql);	
		if($data)
		{
			return $data[0];
		}
		else
		{
			return false;
		}	
	}
	public function getZhaoShang()
	{//获取假的招商信息
		$num = date('H')+1;
		$sql = "SELECT A.GoodsID, A.Title, A.D_title, A.pic, A.Org_Price, A.Price, A.SellerID,A.Quan_price, A.Quan_condition,A.Quan_surplus,A.Quan_receive
FROM  `".C('DB_PREFIX')."dataoke_product` AS A
WHERE A.IsQunNull<>'1' 
LIMIT 18,$num";

		$data = $this->query($sql);	
		//$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}	
	
	}

	
	
	
	

}



?>