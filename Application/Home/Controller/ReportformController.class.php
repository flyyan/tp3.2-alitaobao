<?php
namespace Home\Controller;
use Think\Controller;
class ReportformController extends Controller {
	public function getList()
	{//群号查询，时间查询，标题查询
	
		$upTime = $this->updatexls();	
		$Reportform = new \Admins\Model\ReportformModel();
		$qun = 0;
		$startTime = 0;
		$endTime = 0;
		$payBtn = 0;//申请支付按钮
		if(isset($_GET['qun']) && $_GET['qun']<>'')
		{
			$qun = $_GET['qun'];
		}
		if(isset($_GET['startTime']) && $_GET['startTime']<>'')
		{
			$startTime = $_GET['startTime'];
		}
		if(isset($_GET['endTime']) && $_GET['endTime']<>'')
		{
			$endTime = $_GET['endTime'];
		}	
		if($startTime<>0 && $endTime<>0)
		{
			$startTime = date('Y-m-d H:i:s',strtotime($startTime));
			$endTime = date('Y-m-d H:i:s',strtotime($endTime));
		}

		$res = $Reportform ->getLists($qun,$startTime,$endTime);
		$list = $res['list'];
		$page = $res['page'];
		foreach($list as $k=>$v)
		{
			if($v['orderstatus'] == "订单付款")
			{
				$v["zhifu"] ="<p class='text-muted'>交易进行中</p>"; 
				$payBtn = 0;
				$v["paybtn"] = $payBtn;
			}
			if($v['orderstatus'] == "订单失效")
			{
				$v["zhifu"] ="<p class='text-warning'>交易取消</p>"; 
				$payBtn = 0;
				$v["paybtn"] = $payBtn;
			}
			if($v['orderstatus'] == "订单结算" && $v['paystatus']=="0")
			{
				$v["zhifu"] ="<p class='text-primary'>交易完成，可申请提现</p>";
				$payBtn = 1;
				$v["paybtn"] = $payBtn;
				
			}
			if($v['orderstatus'] == "订单结算" && $v['paystatus']=="1")
			{
				$v["zhifu"] ="<p class='text-danger'>交易完成，提现进行中...</p>"; 
				$payBtn = 0;
				$v["paybtn"] = $payBtn;
			}
			if($v['orderstatus'] == "订单结算" && $v['paystatus']=="2")
			{
				$v["zhifu"] ="<p class='text-success'>交易完成，已提现^_^</p>"; 
				$payBtn = 0;
				$v["paybtn"] = $payBtn;
			}
			$lists[$k] = $v;
		}
		//还有一个统计处理：1交易进行中，2未提现，3在提现，4已提现
		$dealing = $Reportform ->qunPayCount($qun."微信群",1);		
		$unpay = $Reportform ->qunPayCount($qun."微信群",2);
		$paying = $Reportform ->qunPayCount($qun."微信群",3);
		$payed = $Reportform ->qunPayCount($qun."微信群",4);

		$this->assign('dealing',$dealing);
		$this->assign('unpay',$unpay);
		$this->assign('paying',$paying);
		$this->assign('payed',$payed);
		$this->assign('qun',$qun);

		$this->assign('list',$lists);// 赋值数据集
		$this->assign('page',$page);// 赋值分页输出
		$this->assign('uptime',$upTime);// 赋值分页输出
		//var_dump($list);
		if(isMobile()){
			$this->display('getListwap');
		}else{
			$this->display('getList');
		}

	}
	public function updatexls()
	{//每次浏览的时候更新
		//文件名是时间戳
		$Reportform = new \Admins\Model\ReportformModel();
		//$xls = "1497307511.xls";
		$lastTime = $Reportform->getUpddateTime();
		$lastTime = $lastTime['updatetime'];//这里先都去数据库的一个更新时间，初始化一下
	
		//判断制定目录有没有文件
		$dir = "./Uploads/softUp/";
		$xls = "";
		$fileList = getFile($dir);
		foreach($fileList as $k=>$v)
		{
			if(strpos($v,"xls")!=false)
			{
				$xls = $v;
			}
		}
		//更新完后 写入数据库时间		
		if($xls == "") return $lastTime;
				
		$dirFile = $dir.$xls;	
		$result = $Reportform->UpdataXls($dirFile);
		@unlink ($dir.$xls); 

		//更新一下数据库
		//文件名是时间戳
		$houzhui = substr(strrchr($xls, '.'), 1);
		$result = basename($xls,".".$houzhui);		
		$time = date("Y-m-d H:i:s",$result);

		$Reportform->SetUpddateTime($time);
		$lastTime = $time;
		//$this->show("最后更新时间---","utf-8");
		
		
		return $lastTime;
				
	}
	
	
	
	

}