<?php
namespace Home\Controller;
use Think\Controller;
class ProductController extends Controller{
    public function index(){
        $this->show('Product','utf-8');
    }
	public function getData()
	{//获取上传xls的数据输出
		$qun = '2';
		if(isset($_GET['qun']) || $_GET['qun']<>'')
		{
			$qun = $_GET['qun'] ;
			
			$keywords = "";
			if(isset($_GET['keywords']) && $_GET['keywords']<>'')
			{
				$keywords = $_GET['keywords'];
			}
			
			$Product = new \Home\Model\ProductModel();
			$res = $Product->getList($qun,$keywords);
			
		$list = $res['list'];
		$page = $res['page'];
		$this->assign('list',$list);// 赋值数据集
		$this->assign('page',$page);// 赋值分页输出
		//var_dump($list);
		
		$this->display();		
			
		}

	}
	public function getAllData()
	{//获取全部数据
		$qun = '2';
		$pid = '';
		$cid = '0';
		$keywords = "";
		if(isset($_GET['keywords']) && $_GET['keywords']<>'')
		{
			$keywords = $_GET['keywords'];
		}		
		if(isset($_GET['cid']) && $_GET['cid']<>'')
		{
			$cid = $_GET['cid'];
		}		
		
		if(isset($_GET['qun']) || $_GET['qun']<>'')
		{
			$qun = $_GET['qun'] ;
			//通过群id获取pid
			$Promoter = new \Admins\Model\PromoterModel();
			$res = $Promoter->getOneData($qun);
			if($res)
			{
				$result = $res[0];
				$pid = $result['pid'];
			}
			if($pid=='')exit;
			
			//遍历总库的产品

			
			
			$Product = new \Home\Model\ProductModel();
			
			$res = $Product->getAllData($keywords,$cid);
			$list = $res['list'];
			$page = $res['page'];
			$listdata = '';
			foreach($list as $k=>$v)
			{
				$activityId = $v['quan_id'];
				$GoodsID = $v['goodsid'];
				$v['sclick']=='';
				$v['fansKongling']=='';
				$dx = 1;
				if($v['commission_queqiao'] >= $v['commission_jihua'])
				{
					$dx = "";
				}
				
				//$qun 和 $goodsid 有没有sclick 下面的错了
				$result = $Product->getFans($qun,$GoodsID);//获取短连接和淘口令
				if($result)
				{
					$v['sclick'] = $result['sclick'];
					$v['fanskongling'] = $result['fanskongling'];
				}else
				{

					//这个先判断有没有 sclick
					if($v['sclick']=='')
					{
						$fansbuyUrl = get_fansbuyUrl($GoodsID,$activityId,$pid,$dx);
						$sclick = get_sclick($fansbuyUrl);
						//保存一下短连接
						$Product_Admins = new \Admins\Model\ProductModel();
						$data['goodsId'] = $GoodsID;
						$data['quanId'] = $activityId;
						$data['groupId'] = $qun;
						$data['sclick'] = $sclick;
						$data['cdate'] = date('Y-m-d h:i:s');
						$re = $Product_Admins->saveSclick($data);			
						
						$v['sclick'] = 	$sclick;
					}
				
				}
				
				
				$v['commission'] = 	( $v['commission']*0.5);//返佣比例除以2
				$listdata[$k] = $v;		
				
			}
			
			$list = $listdata;
			$this->assign('list',$list);// 赋值数据集
			$this->assign('page',$page);// 赋值分页输出
			
			//$this->show('<h1>各位稍等，修复网站复制不是问题...</h1>','utf-8');
			$this->assign("title",$title);
			$this->assign("qun",$qun);
			if(isMobile())
			{
				$this->display('wap');
			}else
			{
				$this->display('pc');
			}
				
		}
		//$fansbuyurl = "https://uland.taobao.com/coupon/edetail?activityId=b51a87aeb703443b92aea96b5ca3b70e&pid=mm_33549446_18174221_64892431&itemId=521676043030&src=qhkj_dtkp&dx=";
		//$result = get_sclick($fansbuyurl);

		//var_dump($result);
	}
/*
函数注解：
imagecopymerge()

imagecopymerge() 函数用于拷贝并合并图像的一部分，成功返回 TRUE ，否则返回 FALSE 。

语法：

bool imagecopymerge( resource dst_im, resource src_im, int dst_x, int dst_y, int src_x, int src_y, int src_w, int src_h, int pct )

参数说明:

dst_im 目标图像
src_im 被拷贝的源图像
dst_x 目标图像开始 x 坐标
dst_y 目标图像开始 y 坐标，x,y同为 0 则从左上角开始
src_x 拷贝图像开始 x 坐标
src_y 拷贝图像开始 y 坐标，x,y同为 0 则从左上角开始拷贝
src_w （从 src_x 开始）拷贝的宽度
src_h （从 src_y 开始）拷贝的高度
pct 图像合并程度，取值 0-100 ，当 pct=0 时，实际上什么也没做，反之完全合并。
*/	
	public function getImg($img="",$kl = "")
	{
		$goodsid = $img;
		$qcodeImg = "";
		//查询图片
		$Product = new \Home\Model\ProductModel();
		$res = $Product->getImages($goodsid);	
		
		header( "Content-type:image/jpeg");
		vendor('Httplib','','.php');
		$http = new \Httplib($res.'_430x430q90.jpg');
		$http->send();
		$webHtml = $http->response();
		
		if($kl=="")
		{
			echo $webHtml;
			exit;
		}
		else
		{			
			$host = $_SERVER['SERVER_NAME'];
			//二维码的地址是自身网址 1.0版本
			$klUrl = "http://".$host."/index.php/Home/Product/copyKouling?kl=".$kl;
			$codeImg = file_get_contents("http://".$host."/index.php/Home/QRcode/getQRcode?url=".$klUrl);
			
			//下面是php合成图片
			$bigImg = imagecreatefromstring($webHtml);
			$qCodeImg = imagecreatefromstring($codeImg);
			$qCodeWidth = 110;
			$qCodeHight = 110;
			imagecopy($bigImg,$qCodeImg, 320, 320, 0, 0, $qCodeWidth, $qCodeHight);	
			imagejpeg($bigImg);
			imagedestroy($bigImg);
			imagedestroy($qcodeImg);
			exit;
		}


	}
	public function getImgAndQr($img="",$qunid = "")
	{//获取产品图片和二维码的合成图
		$goodsid = $img;
		$qcodeImg = "";
		$qunid = "";
		if(isset($_GET['qunid']) && $_GET['qunid']<>'')
		{
			$qunid  = $_GET['qunid'];
		}
		if(isset($_GET['goodsid']) && $_GET['goodsid']<>'')
		{
			$goodsid  = $_GET['goodsid'];
		}
		//查询图片
		$Product = new \Home\Model\ProductModel();
		$res = $Product->getImages($goodsid);	
		
		header( "Content-type:image/jpeg");
		vendor('Httplib','','.php');
		$http = new \Httplib($res.'_430x430q90.jpg');
		$http->send();
		$webHtml = $http->response();
		
		if($qunid=="" || $goodsid=="")
		{
			echo $webHtml;
			exit;
		}
		else
		{			
			$host = $_SERVER['SERVER_NAME'];
			//很多种方法生成二维码地址：微信自身的提示，百度翻译，有道翻译
			//1.http://rd.wechat.com/qrcode/confirm?block_type=101&content=%EF%BF%A51333332%EF%BF%A5%20%20%E6%88%91%E8%BE%93%E5%87%BA%E7%9A%84%E4%BF%A1%E6%81%AF%EF%BC%81%EF%BC%81&lang=zh_CN&scene=30
//2.

			//二维码的地址是自身网址 1.1版本
			$klUrl = "http://".$host."/index.php/Home/Product/copykl?qunid=".$qunid."&goodsid=".$goodsid;
			$klUrl = urlencode($klUrl);
			//二维码的地址是自身网址 1.0版本
			//$klUrl = "http://".$host."/index.php/Home/Product/copyKouling?kl=".$kl;
			$codeImg = file_get_contents("http://".$host."/index.php/Home/QRcode/getQRcode?url=".$klUrl);
			
			//下面是php合成图片
			$bigImg = imagecreatefromstring($webHtml);
			$qCodeImg = imagecreatefromstring($codeImg);
			$qCodeWidth = 110;
			$qCodeHight = 110;
			imagecopy($bigImg,$qCodeImg, 320, 320, 0, 0, $qCodeWidth, $qCodeHight);	
			imagejpeg($bigImg);
			imagedestroy($bigImg);
			imagedestroy($qcodeImg);
			exit;
		}


	}
	public function fansUrl()
	{//二合一链接跳转
	
	
	}
	public function getZhaoshang()
	{//在产品表里面获取一下假的招商信息
		$Product = new \Home\Model\ProductModel();
		$res = $Product->getZhaoShang();
		//var_dump($res);	
		
		$this->assign('list',$res);
		
		$this->display();
	}
	public function copyKouling()
	{//复制淘口令版本1.0--直接传口令输出
		$kl = "";
		if(isset($_GET['kl']) && $_GET['kl']<>'')
		{
			$kl =$_GET['kl'];
		}
		
		$this->assign('kl',$kl);
		$this->display();
	}
	public function copykl($qunid,$goodsid)
	{//复制淘口令版本1.1--传一个产品id,通过查询数据库，ajax打印一个页面
		$goodsid = "";
		$qunid = "";
		if(isset($_GET['goodsid']) && $_GET['goodsid']<>'')
		{
			$goodsid =$_GET['goodsid'];
		}
		if(isset($_GET['qunid']) && $_GET['qunid']<>'')
		{
			$qunid =$_GET['qunid'];
		}
		$this->display();
	}
	public function shop()
	{//如果群主没时间发了，丢一个链接，让群成员自己购买的页面
		$qun = '2';
		$pid = '';
		$cid = '0';
		$keywords = "";
		if(isset($_GET['keywords']) && $_GET['keywords']<>'')
		{
			$keywords = $_GET['keywords'];
		}
		if(isset($_GET['cid']) && $_GET['cid']<>'')
		{
			$cid = $_GET['cid'];
		}		
		if(isset($_GET['qun']) || $_GET['qun']<>'')
		{
			$qun = $_GET['qun'] ;
			if(is_numeric($qun)==false)exit;
			//通过群id获取pid
			$Promoter = new \Admins\Model\PromoterModel();
			$res = $Promoter->getOneData($qun);
			if($res)
			{
				$result = $res[0];
				$pid = $result['pid'];
			}
			if($pid=='')exit;
			
			//遍历总库的产品
			
			$Product = new \Home\Model\ProductModel();
			
			$res = $Product->getAllData($keywords,$cid);
			$list = $res['list'];
			$page = $res['page'];			
			
			
			
		}	
		$this->assign('list',$list);// 赋值数据集
		$this->assign('page',$page);// 赋值分页输出
		
		//$this->assign("title",$title);
		$this->assign("qun",$qun);		
		$this->display();
	}
	public function share()
	{

		$this->display();
	}


}



?>