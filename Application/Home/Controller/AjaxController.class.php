<?php
namespace Home\Controller;
use Think\Controller;
class AjaxController extends Controller {

    public function index(){

        //$this->show('Ajax','utf-8');
		$goodsid = $_GET['goodsid'];
		if($goodsid=='')
		{
			return false;
		}
		$Ajax = new \Home\Model\AjaxModel();
		$res = $Ajax->CheckQuan($goodsid);
		if(!$res)
		{
			return false;
		}
		$isQuan = check_Quan($res['sellerid'],$res['quan_id']);
		$goodsid = $res['goodsid'];
		$quanid = $res['quan_id'];
		if($isQuan)
		{
			$info = '可用';
			$result = '1';
			$msgs = array('goodsid'=>$goodsid,'quanid'=>$quanid);
		}
		else
		{
			$info = '过期';
			$result = '0';
			$msgs = array('goodsid'=>$goodsid,'quanid'=>$quanid);
		}
		$result = array(
				'result'=>$result,
				'info'=>$info,
				'msgs'=>$msgs	
		);
		echo json_encode($result);
		exit;
    }
	public function getKouling()
	{
	
		$qunid = '';
		$goodsid = '';
		$clickTime = '';//主要防止频繁请求  //1495673417000
		
		$result = 0;
		$info = '';
		$msgs = '';
		
		if(isset($_POST['qunid']) && $_POST['qunid']<>'')
		{
			$qunid = $_POST['qunid'];
		}
		if(isset($_POST['goodsid']) && $_POST['goodsid']<>'')
		{
			$goodsid = $_POST['goodsid'];
		}
		if(isset($_POST['clicktime']) && $_POST['clicktime']<>'')
		{
			$clicktime = $_POST['clicktime'];
		}

		if($qunid=='' || $goodsid =='' || $clicktime == '')
		{
			$result = 3;
			$info = '';
			$msgs = '';	
			$result = array(
				'result'=>$result,
				'info'=>$info,
				'msgs'=>$msgs,
			);
			echo json_encode($result);
			exit;
		}
		//$clickTime = isDateTime($clickTime);
		if($clicktime)
		{
			if(cookie('proTime')<>'')
			{				
				$proTime = cookie('proTime');
				
				if($proTime<>'0')
				{
					if(($clicktime-$proTime)<=2)
					{
						cookie('proTime',$clicktime);
						$result = 0;
						//return "false";
						//exit;					
					}					
				}

				cookie('proTime',$clicktime);

				
			}
			else
			{
				cookie('proTime','0'); 
			}
			
			
		}

		//file_put_contents("test.txt",$qunid.$goodsid);
		
		$Ajax = new \Home\Model\AjaxModel();
		$res = $Ajax->GetKouling($qunid,$goodsid);
//		foreach($res as $k=>$v)
//		{
//			file_put_contents("test11.txt",$k.'---->'.$v,FILE_APPEND);
//		}
		
		if($res)
		{
			if($res['sclick']=='')
			{
				$result = 4;
				$info = '';
				$msgs = '';	
				$result = array(
					'result'=>$result,
					'info'=>$info,
					'msgs'=>$msgs,
				);
				echo json_encode($result);
				exit;
			}
			$msgs = get_fansKouling($res['d_title'],$res['pic'],$res['sclick']);
			if($msgs<>'')
			{
				$result = 1;
				$info = $res['goodsid'];
				$Ajax->UpdateKouling($msgs,$qunid,$goodsid);
				
			}
			$msgs = getConfuseKouling($msgs);
		}

		
		$result = array(
			'result'=>$result,
			'info'=>$info,
			'msgs'=>$msgs,
		);
		echo json_encode($result);
		exit;
	}
	public function jihuaCommission()
	{

		$result = "0";
		$info = "";
		$msgs = "";
		$goodsid = "";
		if(isset($_POST['goodsid']) && $_POST['goodsid']<>'')
		{
			$goodsid = $_POST['goodsid'];
		}
		if($goodsid == "")
		{
			$result = 0;
			$info = '';
			$msgs = '';	
			$result = array(
				'result'=>$result,
				'info'=>$info,
				'msgs'=>$msgs,
			);
			echo json_encode($result);
			exit;
		}
		//有了商品id，这里可以穿大淘客id
		$Ajax = new \Home\Model\AjaxModel();
		$res = $Ajax->GetDTKid($goodsid);
		if($res)
		{
			$DTKid = $res["id"];
			$result = "1";
			$info = $DTKid;
			$msgs = "succes";
		}
		
		
		$result = array(
			'result'=>$result,
			'info'=>$info,
			'msgs'=>$msgs,
		);
		echo json_encode($result);
		exit;	
	}
	public function applyPay()
	{
		$qun = '';
		$goodsid = '';
		$ordernumber = '';
		
		$result = 0;
		$info = '';
		$msgs = '';
		
		if(isset($_POST['qun']) && $_POST['qun']<>'')
		{
			$qun = $_POST['qun'];
		}
		if(isset($_POST['goodsid']) && $_POST['goodsid']<>'')
		{
			$goodsid = $_POST['goodsid'];
		}
		if(isset($_POST['ordernumber']) && $_POST['ordernumber']<>'')
		{
			$ordernumber = $_POST['ordernumber'];
		}

		if($qun=='' || $goodsid =='' || $ordernumber == '')
		{
			$result = 3;
			$info = '';
			$msgs = '';	
			$result = array(
				'result'=>$result,
				'info'=>$info,
				'msgs'=>$msgs,
			);
			echo json_encode($result);
			exit;
		}
		//下面是处理晚上19:00~00点是支付时间，不允许申请提现
		date_default_timezone_set('PRC');
		$timeNow = date('Y-m-d H:i:s');
		$time=date('Y-m-d ');
		$timeStart = $time.'19:00:00';
		$timeEnd = $time.'23:59:59';
		if(strtotime($timeStart) < strtotime($timeNow) && strtotime($timeNow)<strtotime($timeEnd))
		{
			$result = array(
				'result'=>"1",
				'info'=>"付款时间(每天19:00~23:59:59),\n\n暂停申请(请谅解!),请明天申请^_^",
				'msgs'=>"PayTime",
			);
			echo json_encode($result);
			exit;
		}

		//下面时真正的数据处理	
		$Ajax = new \Home\Model\AjaxModel();
		$qun = $qun."微信群";
		$res = $Ajax->ApplyPay($goodsid,$ordernumber,$qun);	
		if($res)
		{
			$result = "1";
			$info = "申请成功，等待或通知管理员";
			$msgs = "succes";
		}
			
		$result = array(
			'result'=>$result,
			'info'=>$info,
			'msgs'=>$msgs,
		);
		echo json_encode($result);
		exit;	
		
		
	}
	public function getQRcode()
	{//没用
	
		$goodsid = $img;
		//查询图片
		$Product = new \Home\Model\ProductModel();
		$res = $Product->getImages($goodsid);	
		
		header( "Content-type:image/jpeg");
		vendor('Httplib','','.php');
		$http = new \Httplib($res.'_430x430q90.jpg');
		$http->send();
		$webHtml = $http->response();
		echo $webHtml;
		exit;
		
			$host = $_SERVER['SERVER_NAME'];
			$klUrl = "http://".$host."/index.php/Home/Product/copyKouling?kl=".$kl;
			$codeImg = file_get_contents("http://".$host."/index.php/Home/QRcode/getQRcode?url=http://www.baidu.com");
			
			//下面是php合成图片
			$bigImg = imagecreatefromstring($webHtml);
			$qCodeImg = imagecreatefromstring($codeImg);
			$qCodeWidth = 130;
			$qCodeHight = 130;
			imagecopy($bigImg,$qCodeImg, 300, 300, 0, 0, $qCodeWidth, $qCodeHight);	
			imagejpeg($bigImg);	
			imagedestroy($bigImg);
			imagedestroy($qcodeImg);		
			exit;	
	
	}


}

?>