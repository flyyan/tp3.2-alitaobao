<?php
return array(
	//'配置项'=>'配置值'
	
	'SESSION_AUTO_START' =>true,
	
//	'SHOW_PAGE_TRACE'=>true,//开启trace显示界面，便于调试
	
	//'URL_MODEL'=>2,
	
	'TMPL_L_DELIM'    =>    '<{',
	'TMPL_R_DELIM'    =>    '}>',

// 开启路由
	'URL_ROUTER_ON'   => true,
	'URL_ROUTE_RULES'=>array(
		'qun/:qun'          => '/index.php/Home/Product/getAllData/qun/:1',
	),
	'DB_TYPE'   => 'mysql', // 数据库类型
	'DB_HOST'   => 'localhost', // 服务器地址
	'DB_NAME'   => 'alitaobao', // 数据库名
	'DB_USER'   => 'root', // 用户名
	'DB_PWD'    => '', // 密码
	'DB_PORT'   => 3306, // 端口
	'DB_PREFIX' => 'think_', // 数据库表前缀

//	'DB_TYPE'   => 'mysql', // 数据库类型
//	'DB_HOST'   => 'qdm112527208.my3w.com', // 服务器地址
//	'DB_NAME'   => 'qdm112527208_db', // 数据库名
//	'DB_USER'   => 'qdm112527208', // 用户名
//	'DB_PWD'    => 'tao13239131886', // 密码
//	'DB_PORT'   => 3306, // 端口
//	'DB_PREFIX' => 'think_', // 数据库表前缀

	//自定义的xls文件字段读取
	
	'XLSFIELD' =>  array(
		'goodsId' =>'商品id',
		'taokeShortUrl'=>'淘宝客短链接(300天内有效)',
		'quanUrl'=>'优惠券链接',
		'taokeUrl'=>'淘宝客链接',
		'taoKouling'=>'淘口令(300天内有效)',
		'quanKouling'=>'优惠券淘口令(300天内有效)',
		'quanShortUrl'=>'优惠券短链接(300天内有效)',
		'Price'=>'商品价格(单位：元)',//数据库无此字段
		'quanPrice'=>'优惠券面额',
		'Picimg'=>'商品主图',//次字段不存数据库
		'goodsTitle'=>'商品名称',//次字段不存数据库
		),
	//d读取报表的字段
	'reportform'=>array(
		'clickTime'=>'点击时间',
		'goodsTitle'=>'商品信息',
		'goodsId'=>'商品ID',
		'shopName'=>'所属店铺',
		'goodsNums'=>'商品数',
		'goodsPrice'=>'商品单价',
		'orderStatus'=>'订单状态',
		'splitRatio'=>'分成比率',
		'paymentAmount'=>'付款金额',
		'settlementAmount'=>'结算金额',
		'settlementTime'=>'结算时间',
		'commissionRate'=>'佣金比率',
		'commissionAmount'=>'佣金金额',
		'orderNumber'=>'订单编号',
		'groupName'=>'广告位名称',
		'orderType'=>'订单类型',
	
	),
	'TaobaoApiKey'=>array(
		'TaobaoKey1'=>array(
			'appkey'=>'23491903',
			'secretKey'=>'a05621ce6d64bc1df1ff0f57c6fa5f7e',
		),
		'TaobaoKey2'=>array(
			'appkey'=>'2',
			'secretKey'=>'22'	
		),
		'TaobaoKey3'=>array(
			'appkey'=>'23491903',
			'secretKey'=>'a05621ce6d64bc1df1ff0f57c6fa5f7e',
		),
		'TaobaoKey4'=>array(
			'appkey'=>'23491903',
			'secretKey'=>'a05621ce6d64bc1df1ff0f57c6fa5f7e',
		),	
	),
	'DataokeProductCid'=>array(
	//商品分类
		'0'=>'全部',
		'1'=>'女装',
		'9'=>'男装',
		'10'=>'内衣',
		'2'=>'母婴',
		'3'=>'化妆品',
		'4'=>'居家',
		'5'=>'鞋包配饰',
		'6'=>'美食',
		'7'=>'文体车品',
		'8'=>'数码家电',
	),	
	'QingtaokeProductCat'=>array(
	//商品分类
		'0'=>'全部',
		'1'=>'母婴',
		'2'=>'美妆',
		'3'=>'居家',
		'4'=>'鞋包配饰',
	),

);