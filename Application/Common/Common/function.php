<?php
/**
 * Goofy 2011-11-30
 * getDir()去文件夹列表，getFile()去对应文件夹下面的文件列表,二者的区别在于判断有没有“.”后缀的文件，其他都一样
 */
 
//获取文件目录列表,该方法返回数组
function getDir($dir) {
    $dirArray[]=NULL;
    if (false != ($handle = opendir ( $dir ))) {
        $i=0;
        while ( false !== ($file = readdir ( $handle )) ) {
            //去掉"“.”、“..”以及带“.xxx”后缀的文件
            if ($file != "." && $file != ".."&&!strpos($file,".")) {
                $dirArray[$i]=$file;
                $i++;
            }
        }
        //关闭句柄
        closedir ( $handle );
    }
    return $dirArray;
}
 
//获取文件列表
function getFile($dir) {
    $fileArray[]=NULL;
    if (false != ($handle = opendir ( $dir ))) {
        $i=0;
        while ( false !== ($file = readdir ( $handle )) ) {
            //去掉"“.”、“..”以及带“.xxx”后缀的文件
            if ($file != "." && $file != ".."&&strpos($file,".")) {
                $fileArray[$i]=iconv("GBK","UTF-8",$file);
                if($i==100){
                    break;
                }
                $i++;
            }
        }
        //关闭句柄
        closedir ( $handle );
    }
    return $fileArray;
}
 
//调用方法getDir("./dir")……

function getArrKey($arr,$ruleField)
{//通过表头标题查找表列
	$res = '';
	foreach($ruleField as $key=>$val)
	{
	
		foreach($arr as $k=>$v)
		{			
			if($val==$v)
			{
				$res[$key] = $k;
			}
		}
	}
	return $res;
}

function get_extension($file)
{
	return pathinfo($file, PATHINFO_EXTENSION);
}
function get_basename($filename)
{
	//return preg_replace('/^.+[\\\\\\/]/', '', $filename);
	$houzhui = substr(strrchr($filename, '.'), 1);
	$res = str_replace('.'.$houzhui, '', $filename);
	return $res;
}
function get_fansbuyUrl($GoodsID,$activityId,$pid,$dx="")
{
	$str = "http://uland.taobao.com/coupon/edetail?activityId=$activityId&pid=$pid&itemId=$GoodsID&src=qtka_qjbb&dx=$dx";
	return $str;
}
function get_fansKouling($goodsName='',$logo='',$url='')
{
//$Tb = c("TaobaoApiKey");//可以随机
//print_r(count($Tb) );

	if($logo==''|| $url=='')
	{
		return false;
	}
	vendor('taobao.fileTest','','.php');
		
    $c = new \TopClient;
	$TaobaoKey = c("TaobaoApiKey.TaobaoKey1");
    $c->appkey = $TaobaoKey['appkey'];
    $c->secretKey = $TaobaoKey['secretKey'];

	$req = new \TbkTpwdCreateRequest;
	$req->setUserId("33549446");
	$req->setText($goodsName);
	$req->setUrl($url);
	$req->setLogo($logo);
	$req->setExt("{}");
	$resp = $c->execute($req);

	if($resp->data->model<>'')
	{
		return (string)$resp->data->model;
	}
	return false;
}
function get_sclick($fansbuyurl)
{//获取淘宝短连接
	if($fansbuyurl=='')
	{
		return false;
	}
	vendor('taobao.fileTest','','.php');
		
    $c = new \TopClient;
	$TaobaoKey = c("TaobaoApiKey.TaobaoKey1");	
    $c->appkey = $TaobaoKey['appkey'];
    $c->secretKey = $TaobaoKey['secretKey'];
	
	$req = new \TbkSpreadGetRequest;
	$requests = new \TbkSpreadRequest;
	$requests->url=$fansbuyurl;
	$req->setRequests(json_encode($requests));
	$resp = $c->execute($req);
	if($resp)
	{
		$msg = (string)$resp->results->tbk_spread->err_msg;
		if($msg=="OK")
		{
			$sclick = (string)$resp->results->tbk_spread->content;
		}
	}
	return $sclick;
	
}
function get_shortUrl($longUrl)
{
	$longUrl = urlencode($longUrl);
	$json_string=file_get_contents("http://api.t.sina.com.cn/short_url/shorten.json?source=3271760578&url_long=$longUrl"); 
	$obj=json_decode($json_string);
	
	$obj = $obj[0];
	return $obj->url_short;

}
//获得二合一长连接
//http://uland.taobao.com/coupon/edetail?activityId=7a4930f49f1d4261be63f349180f70a4&pid=mm_123_123_123&itemId=536766964439&src=pgy_pgyqf &dx=1

//计划优先 http://uland.taobao.com/coupon/edetail?activityId=13d7f7d41b6e4f199155199745180232&pid=mm_33549446_18174221_64894331&itemId=539617875666&src=qtka_qjbb&dx=1

//高佣金优先
//http://uland.taobao.com/coupon/edetail?activityId=13d7f7d41b6e4f199155199745180232&pid=mm_33549446_18174221_64894331&itemId=539617875666&src=qtka_qjbb&dx=

//短网址接口
//http://api.t.sina.com.cn/short_url/shorten.xml?source=3271760578&url_long=http://cc8w.com
//http://api.t.sina.com.cn/short_url/shorten.json?source=3271760578&url_long=http://www.douban.com/note/249723561/
//我才注册的新浪开放平台
//App Key：2726475570
//App Sercet：fd2c674c9c3bbece97cbbbe3bb9bb8a3

//http://api.t.sina.com.cn/short_url/shorten.xml?source=2726475570&url_long=http://cc8w.com


//http://api.m.taobao.com/h5/mtop.taobao.wireless.share.password.getpasswordshareinfo/1.0/?v=1.0&api=mtop.taobao.wireless.share.password.getpasswordshareinfo&appKey=12574478&t=1483287905123&type=json&sign=10d5d90c4a545fea90ca70e3b79c5f2e&data=%7B"url"%3A"http%3A%2F%2Fuland.taobao.com%2Fcoupon%2Fedetail%3FactivityId%3D13d7f7d41b6e4f199155199745180232%26pid%3Dmm_33549446_18174221_64894331%26itemId%3D539617875666%26src%3Dqtka_qjbb%26dx%3D1"%2C"passwordType"%3A"tao"%2C"sourceType"%3A"other"%2C"title"%3A"欧利时星光璀璨玫瑰金真皮带女士手表女表韩版时尚潮流防水石英表"%2C"bizId"%3A"mama-taobaolianmeng"%7D


function check_Quan2($sellerid,$quanid)
{//检查券是否可用  API taobao.promotion.coupondetail.get
	//$url = "http://shop.m.taobao.com/shop/coupon.htm?seller_id=$sellerid&activity_id=$quanid";
	$url = "http://shop.m.taobao.com/shop/coupon.htm?activityId=$quanid&sellerId=$sellerid";

	vendor('Httplib','','.php');
	$http = new \Httplib($url);
	$http->send();
	$webHtml = $http->response();
	//echo $webHtml;
	//file_put_contents("test12.html",$webHtml);
	$res = strpos($webHtml,"不存在");
	if($res==false)
	{
		return true;//券可用
	}
	else
	{
		return false;//券不可用
	}

}
function check_Quan($sellerid,$quanid)
{//解决了阿里的浏览器或ip封锁
//cc8w.com/checkQuan.php?activityid=8ef037fc2ac1447b890f1ec966fc589b&sellerid=431360643
	$url = "http://cc8w.com/checkQuan.php?activityid=$quanid&sellerid=$sellerid";
	$result = file_get_contents($url);
	$obj=json_decode($result); 
	$re = $obj->result;
	if($re)
	{
		return true;//券可用
	}
	else
	{
		return false;//券不可用
	}
}
function isDateTime($dateTime)
{//是否为时间格式
    $ret = strtotime($dateTime);
    return $ret !== FALSE && $ret != -1;
}

function qrcode($url='http://www.baidu.com',$level=3,$size=2)
{
  
	  Vendor('phpqrcode.phpqrcode');
	  
	  $errorCorrectionLevel =intval($level) ;//容错级别 
	  $matrixPointSize = intval($size);//生成图片大小 
	//生成二维码图片 
	  //echo $_SERVER['REQUEST_URI'];
	  $object = new \QRcode();
	  return $object->png($url, false, $errorCorrectionLevel, $matrixPointSize, 2);   

}
/*移动端判断*/
function isMobile()
{ 
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
    {
        return true;
    } 
    // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset ($_SERVER['HTTP_VIA']))
    { 
        // 找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
    } 
    // 脑残法，判断手机发送的客户端标志,兼容性有待提高
    if (isset ($_SERVER['HTTP_USER_AGENT']))
    {
        $clientkeywords = array ('nokia',
            'sony',
            'ericsson',
            'mot',
            'samsung',
            'htc',
            'sgh',
            'lg',
            'sharp',
            'sie-',
            'philips',
            'panasonic',
            'alcatel',
            'lenovo',
            'iphone',
            'ipod',
            'blackberry',
            'meizu',
            'android',
            'netfront',
            'symbian',
            'ucweb',
            'windowsce',
            'palm',
            'operamini',
            'operamobi',
            'openwave',
            'nexusone',
            'cldc',
            'midp',
            'wap',
            'mobile'
            ); 
        // 从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT'])))
        {
            return true;
        } 
    } 
    // 协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT']))
    { 
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))
        {
            return true;
        } 
    } 
    return false;
}
/*由于腾讯封杀微信号严重，口令变化下
由￥Ic9A0WDofTq￥改为￥Ic9A0WDofTq《
*/
function getConfuseKouling($kl)
{
	$num = rand(1,2);
	$newkl = '';
	$kl = trim($kl);
	$kl = str_replace("￥","",$kl);
	$kl = str_replace("《"," ",$kl);
	$newStr = trim($kl);

	if($num == 1)
	{//是1改第一个字符		
		$newkl = "￥".$newStr."《";
	}else
	{//是2改最后一个字符
		$newkl = "《".$newStr."￥";
	}
	return $newkl;	
}
function getShopName()
{//一半  根据商家id差商家名称

	$c = new TopClient;
	$c->appkey = $appkey;
	$c->secretKey = $secret;
	$req = new ShoprecommendShopsGetRequest;
	$req->setSellerId("123456");
	$req->setRecommendType("1");
	$req->setCount("3");
	$req->setExt("额外参数");
	$resp = $c->execute($req);

}











?>