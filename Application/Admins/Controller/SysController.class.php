<?php
namespace Admins\Controller;
use Common\Controller\CommController;
class SysController extends CommController{
    public function index(){
        $this->show('SysController模块','utf-8');
    }
	public function Login()
	{
		//$this->show('UserController模块登录Login控制器','utf-8');
		if(!empty($_POST))
		{
			$imgRes = $this->check_verify($_POST['verifyimg']);
			if(!$imgRes)
			{
				$this->error('验证码错误...','../../../../index.php/Admins/',1);
			}
				

			
			if(isset($_POST['username']) && $_POST['username']<>'')
			{
				$User = new \Admins\Model\SysModel();
				$res = $User->userLogin($_POST['username'],$_POST['password']);
				if($res)
				{
					cookie('username',$res['username'],9000); 					
					$this->success('登录成功跳转中...','../../../../index.php/Admins/',2);
					exit;
				}
				else
				{
					
					$this->error('登录失败跳转中...','../../../../index.php/Admins/',2);
				}
				
			
			}
			
			
			
		}

		$this->display();
	}
	
	public function  Loginout()
	{
		cookie('username',null);
		$this->error('退出成功...','../../../../index.php/Admins/',2);
	}
	public function VerifyImg()
	{
		$Verify = new \Think\Verify();
		$Verify->entry();
	}
	public function check_verify($code, $id = '')
	{
		$verify = new \Think\Verify();
		return $verify->check($code, $id);
	}
	
}



?>