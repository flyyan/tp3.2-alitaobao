<?php
namespace Admins\Controller;
use Think\Controller;
class UploadController extends Controller {

	public function index()
	{
		$this->show("这是上传首页","utf-8");
	}
	public function uploadify()
	{
		/*
		Uploadify
		Copyright (c) 2012 Reactive Apps, Ronnie Garcia
		Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
		*/		
		// Define a destination
		
		$targetFolder = '/Uploads'; // Relative to the root
		$verifyToken = md5('unique_salt' . $_POST['timestamp']);			
		if (!empty($_FILES) && $_POST['token'] == $verifyToken)
		{
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
			$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];
			
			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif','png','xls'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			
			if (in_array($fileParts['extension'],$fileTypes))
			{
				//file_put_contents("log1.txt", $targetFile);
				move_uploaded_file($tempFile,$targetFile);
				echo '1';
			}
			else
			{
				echo 'Invalid file type.';
			}
		}
	
	
	}
	
	public function checkExists()
	{
		/*
		Uploadify
		Copyright (c) 2012 Reactive Apps, Ronnie Garcia
		Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
		*/
		
		// Define a destination
		$targetFolder = '/Uploads'; // Relative to the root and should match the upload folder in the uploader script
		
		if (file_exists($_SERVER['DOCUMENT_ROOT'] . $targetFolder . '/' . $_POST['filename'])) {
			echo 1;
		} else {
			echo 0;
		}	
	
	}

	 
	public function upload(){
		$upload = new \Think\Upload();// 实例化上传类
		$upload->maxSize   =     3145728 ;// 设置附件上传大小
		$upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		$upload->savePath  =      '/Uploads'; // 设置附件上传目录
		// 上传文件 
		$info   =   $upload->upload();
		if(!$info) {// 上传错误提示错误信息
			$this->error($upload->getError());
		}else{// 上传成功
			$this->success('上传成功！');
		}
	}

	public function uploadReport()
	{
		$targetFolder = '/Reportform'; // Relative to the root
		$verifyToken = md5('unique_salt' . $_POST['timestamp']);			
		if (!empty($_FILES) && $_POST['token'] == $verifyToken)
		{
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
			$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];
			
			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif','png','xls'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			
			if (in_array($fileParts['extension'],$fileTypes))
			{
				//file_put_contents("log1.txt", $targetFile);				
				move_uploaded_file($tempFile,$targetFile);
				session('reportFile',$_FILES['Filedata']['name']);		
				echo '1';
			}
			else
			{
				echo 'Invalid file type.';
			}
		}
	
	
	}















}

?>
