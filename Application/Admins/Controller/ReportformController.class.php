<?php
namespace Admins\Controller;
use Common\Controller\BaseController;

class ReportformController extends BaseController
{

	public function _initialize()
	{
		$this->checkCookie();
		set_time_limit(900);
		ini_set("memory_limit", "1024M");
		vendor('PHPExcel.PHPExcel','','.php');
	}
	public function getReportformData()
	{//群号查询，时间查询，标题查询
		$Reportform = new \Admins\Model\ReportformModel();
		$res = $Reportform ->getLists();
		$list = $res['list'];
		$page = $res['page'];
		$this->assign('list',$list);// 赋值数据集
		$this->assign('page',$page);// 赋值分页输出
		var_dump($list);
		//$this->display();
	}
	
	public function updateReportformData()
	{
		$reportFile = '';
		if(count($_POST)>0)
		{
			foreach($_POST as $k=>$v)
			{
				$_POST['reportFile'] = $v;
			}
		}
		if(isset($_POST['reportFile']) && $_POST['reportFile']<>'')
		{
			$reportFile = $_POST['reportFile'];
		}
		if($reportFile == '')
		{
			$this->error('操作失败无文件');
		}
		//echo $reportFile;
		$root = dirname(THINK_PATH);
		$dirFile = $root."/Reportform/".$reportFile;

		$Reportform = new \Admins\Model\ReportformModel();
		
		$result = $Reportform->UpdataXls($dirFile);
		@unlink ($dirFile); 
		if($result)
		{
			$this->show("数据更新成功","utf-8");
			exit;
		}
		else
		{
			$this->show("数据更新失败","utf-8");
			exit;
		}
		

	}
	public function Uploads()
	{
		$this->display();
	}
	public function ReportformFileList()
	{
	
		$dir = "./Reportform";
		$arrFile = getFile($dir);
		//var_dump($arrFile);
		foreach ($arrFile as $key=>$val)
		{
			
			if(get_extension($val)=="xls")
			{
				$xlsfiles[$key]['file'] = $val;
			}
		}
		
		$this->assign('xlsfiles',$xlsfiles);
		$this->display();
	}
	public function delFile($file)
	{
		@unlink ("./Reportform/".$file); 
		$this->redirect('./Admins/Reportform/ReportformFileList/','' , 1, '已操作,在跳转...');
	}
	public function getListsInfo()
	{
		$qun = "0";
		$payStatus='';
		$payBtn = 0;//申请支付按钮
		if(isset($_GET['qun']) && $_GET['qun']<>"")
		{
			$qun = $_GET['qun']."微信群";
		}
		if(isset($_GET['payStatus']) && $_GET['payStatus']<>"")
		{
			$payStatus = $_GET['payStatus'];
		}
		$Reportform = new \Admins\Model\ReportformModel();
		$listsinfo = $Reportform->getListsInfo($qun,$payStatus);
		$list = $listsinfo['list'];
		$page = $listsinfo['page'];

		foreach($list as $k=>$v)
		{
			if($v['orderstatus'] == "订单付款")
			{
				$v["zhifu"] ="<p class='text-muted'>交易进行中</p>"; 
				$payBtn = 0;
				$v["paybtn"] = $payBtn;
			}
			if($v['orderstatus'] == "订单失效")
			{
				$v["zhifu"] ="<p class='text-warning'>交易取消</p>"; 
				$payBtn = 0;
				$v["paybtn"] = $payBtn;
			}
			if($v['orderstatus'] == "订单结算" && $v['paystatus']=="0")
			{
				$v["zhifu"] ="<p class='text-primary'>交易完成，可申请提现</p>";
				$payBtn = 0;
				$v["paybtn"] = $payBtn;
				
			}
			if($v['orderstatus'] == "订单结算" && $v['paystatus']=="1")
			{
				$v["zhifu"] ="<p class='text-danger'>交易完成，提现进行中...</p>"; 
				$payBtn = 1;
				$v["paybtn"] = $payBtn;
			}
			if($v['orderstatus'] == "订单结算" && $v['paystatus']=="2")
			{
				$v["zhifu"] ="<p class='text-success'>交易完成，已提现^_^</p>"; 
				$payBtn = 0;
				$v["paybtn"] = $payBtn;
			}
			$lists[$k] = $v;
		}





		//还有一个统计处理：1交易进行中，2未提现，3在提现，4已提现
		$dealing = $Reportform ->qunPayCount($qun,1);		
		$unpay = $Reportform ->qunPayCount($qun,2);
		$paying = $Reportform ->qunPayCount($qun,3);
		$payed = $Reportform ->qunPayCount($qun,4);

		$this->assign('dealing',$dealing);
		$this->assign('unpay',$unpay);
		$this->assign('paying',$paying);
		$this->assign('payed',$payed);
		
		$this->assign('list',$lists);// 赋值数据集
		$this->assign('page',$page);// 赋值分页输出
		$this->display();		
	}
	public function DelReportform()
	{
		$date = "";
		if(isset($_POST['datepicker1']) && $_POST['datepicker1']<>"")
		{
			$date = $_POST['datepicker1'];
			
			$datepicker =  date("Y-m-d H:i:s",strtotime($date));
		
			$Reportform = new \Admins\Model\ReportformModel();
			$result = $Reportform->DelReportform($datepicker);
		}

	
		$this->display();
	}
















}
?>