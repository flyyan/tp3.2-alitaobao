<?php
namespace Admins\Controller;
use Common\Controller\BaseController;
class AjaxController extends BaseController {
    public function index(){
       // $this->show('另一个分组的内容','utf-8');
		$this->display();
    }
	public function payEd()
	{
		$qun = '';
		$goodsid = '';
		$ordernumber = '';
		
		$result = 0;
		$info = '';
		$msgs = '';
		
		if(isset($_POST['qun']) && $_POST['qun']<>'')
		{
			$qun = $_POST['qun'];
		}
		if(isset($_POST['goodsid']) && $_POST['goodsid']<>'')
		{
			$goodsid = $_POST['goodsid'];
		}
		if(isset($_POST['ordernumber']) && $_POST['ordernumber']<>'')
		{
			$ordernumber = $_POST['ordernumber'];
		}

		if($qun=='' || $goodsid =='' || $ordernumber == '')
		{
			$result = 3;
			$info = '';
			$msgs = '';	
			$result = array(
				'result'=>$result,
				'info'=>$info,
				'msgs'=>$msgs,
			);
			echo json_encode($result);
			exit;
		}
		//下面时真正的数据处理	
		$Ajax = new \Admins\Model\AjaxModel();
		$qun = $qun."微信群";
		$res = $Ajax->PayEd($goodsid,$ordernumber,$qun);	
		if($res)
		{
			$result = "1";
			$info = "提现成功，请尽快汇钱给代理";
			$msgs = "succes";
		}
			
		$result = array(
			'result'=>$result,
			'info'=>$info,
			'msgs'=>$msgs,
		);
		echo json_encode($result);
		exit;	
			
	
	}
	public function delPayInfo()
	{
		$qun = '';
		$goodsid = '';
		$ordernumber = '';
		
		$result = 0;
		$info = '';
		$msgs = '';
		
		if(isset($_POST['qun']) && $_POST['qun']<>'')
		{
			$qun = $_POST['qun'];
		}
		if(isset($_POST['goodsid']) && $_POST['goodsid']<>'')
		{
			$goodsid = $_POST['goodsid'];
		}
		if(isset($_POST['ordernumber']) && $_POST['ordernumber']<>'')
		{
			$ordernumber = $_POST['ordernumber'];
		}

		if($qun=='' || $goodsid =='' || $ordernumber == '')
		{
			$result = 3;
			$info = '';
			$msgs = '';	
			$result = array(
				'result'=>$result,
				'info'=>$info,
				'msgs'=>$msgs,
			);
			echo json_encode($result);
			exit;
		}	
		$Ajax = new \Admins\Model\AjaxModel();
		$qun = $qun."微信群";
		$res = $Ajax->delPayInfo($goodsid,$ordernumber,$qun);	
		if($res)
		{
			$result = "1";
			$info = "删除了数据！";
			$msgs = "succes";
		}
			
		$result = array(
			'result'=>$result,
			'info'=>$info,
			'msgs'=>$msgs,
		);
		echo json_encode($result);
		exit;	
	
	
	
	}
}

?>