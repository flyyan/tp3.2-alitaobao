<?php
namespace Admins\Controller;
use Common\Controller\BaseController;

class DataokeApiController extends BaseController{

	public function index()
	{
//		$DataokeApi = new \Admins\Model\DataokeApiModel();
//		$res = $DataokeApi->insertData($page='2');
//		var_dump($res);
		
		$this->show("默认的方法","utf-8");
	}
	public function call()
	{
		
		$DataokeApi = new \Admins\Model\DataokeApiModel();
		$DataokePages = $this->getPages();
		$listData = $this->getData(1);
		$data = '';
//新定义一个录入数据库的数组
		foreach($listData as $k=>$v)
		{
			$product['ID'] = $v['ID'];
			$product['GoodsID'] = $v['GoodsID'];
			$product['Title'] = $v['Title'];
			$product['D_title'] = $v['D_title'];
			$product['Pic'] = $v['Pic'];
			$product['Cid'] = $v['Cid'];
			$product['Org_Price'] = $v['Org_Price'];
			$product['Price'] = $v['Price'];
			$product['IsTmall'] = $v['IsTmall'];
			$product['Sales_num'] = $v['Sales_num'];
			$product['Dsr'] = $v['Dsr'];
			$product['SellerID'] = $v['SellerID'];
			$product['Commission'] = $v['Commission'];
			$product['Commission_jihua'] = $v['Commission_jihua'];
			$product['Commission_queqiao'] = $v['Commission_queqiao'];
			$product['Jihua_link'] = $v['Jihua_link'];
			$product['Que_siteid'] = $v['Que_siteid'];
			$product['Jihua_shenhe'] = $v['Jihua_shenhe'];
			$product['Introduce'] = $v['Introduce'];
			$product['Quan_id'] = $v['Quan_id'];
			$product['Quan_price'] = $v['Quan_price'];
			$product['Quan_time'] = $v['Quan_time'];
			$product['Quan_surplus'] = $v['Quan_surplus'];
			$product['Quan_receive'] = $v['ID'];
			$product['Quan_condition'] = $v['Quan_condition'];
			$product['Quan_m_link'] = $v['Quan_m_link'];
			$product['Quan_link'] = $v['Quan_link'];
			$product['Ctime'] = date('Y-m-d H:i:s');
			
			$data[$k] = $product;
		}

//------------------------------
		$sql = '';	
		foreach($data as $v)
		{

			$isNewdata = $DataokeApi->onlyCheck($v['GoodsID']);
			if($isNewdata)
			{//更新操作语句
				//接下来如果券一样的话不进行任何操作，券不一样了再更新
				if(!($DataokeApi->quanOnlyCheck($v['GoodsID'],$v['Quan_id'])))
				{
					$sql .= $DataokeApi->getUpdateSql($v).';';
				}
			}
			else
			{//插入操作语句
				
				$sql .= $DataokeApi->getInsertSql($v).';';
				
				
			}		
		}
		//echo $sql;
		
		//定义一个数组，把这些sql语句以一个引用（指针）传过去，不然直接把字符串当参数太大，太消耗内存了。
		$arraySql[] = $sql;
		//echo $arraySql;
		$result = $DataokeApi->moreExecute($arraySql);
		var_dump($result);
	
	
	}
	public function lists()
	{
		$title = "DataokeApi->list";
		$DataokeApi = new \Admins\Model\DataokeApiModel();
	
		
		$res = $DataokeApi->getList();
		$list = $res['list'];
		$page = $res['page'];
		//var_dump($list);
		$this->assign('list',$list);// 赋值数据集
		$this->assign('page',$page);// 赋值分页输出
		
		
		$this->assign("title",$title);
		$this->display();
	}
	public function getDataokeUrl($page = 1)
	{//同过方法处理大淘客的Url
		$dataokeUrl = "http://api.dataoke.com/index.php?r=Port/index&type=total&appkey=motzfcyc8h&v=2&page=".$page;
		//echo $dataokeUrl;
		return $dataokeUrl;
	}
	public function getPages()
	{//获取大淘客总页数，作为循环次数
	
		//这里要写如果获取数据错误的处理，暂时未写
		$getJson = file_get_contents("http://api.dataoke.com/index.php?r=Port/index&type=total&appkey=motzfcyc8h&v=2&page=1");
		$data = json_decode($getJson,TRUE);
		//var_dump($data);
		$head_data = $data['data'];
		$total_num = $head_data['total_num'];
		return  ceil($total_num/50);	
	}
	public function getData($page=1)
	{
		//这里要写如果获取数据错误的处理，暂时未写
		$opts = array(   
			  'http'=>array(   
				'method'=>"GET",   
				'timeout'=>5,//单位秒  
			   )   
			);  
		$cnt=0;   
//		while($cnt<5 && ($getJson=file_get_contents($this->getDataokeUrl($page), false, stream_context_create($opts)))===FALSE) 
//		{$cnt++; 
//		echo "执行$cnt次"; 
//		flush();
//		} 

	//echo $this->getDataokeUrl($page);
		while($cnt<5 && ($getJson=file_get_contents($this->getDataokeUrl($page), false, stream_context_create($opts)))===FALSE) $cnt++; 


		
	
		
		//var_dump($getJson);
		if($getJson)
		{//正确获取到数据
			$data = json_decode($getJson,TRUE);	
			$result = $data['result'];
		}
		else
		{//错误未获取到数据
			$result = "";
		}

		//var_dump($result);
		return $result;
		
	}
	public function productSyn()
	{
		$this->display();
	}
	public function dataSyn()
	{
		unset($_SESSION['dataokePages']);
		$DataokePages = $this->getPages();
	//$DataokePages= 10;
		for($i=1;$i<=$DataokePages;$i++)
		{
			$_SESSION['dataokePages']['page'.$i] = $i;
		}
		$this->redirect('./Admins/DataokeApi/startSyn','' , 1, '页面跳转中...');	
	
		
	}
	public function startSyn()
	{
		//var_dump($_SESSION['dataokePages']);
		foreach($_SESSION['dataokePages'] as $k=>$v)
		{
			echo  "<b>【".$k."】</b>--";
		}
		
			
		foreach($_SESSION['dataokePages'] as $k=>$v)
		{
			if($v<>NULL)
			{
				
				$DataokeApi = new \Admins\Model\DataokeApiModel();
				$res = $DataokeApi->insertData($v);
				$flag = $k;
				break;
			}
		}
		
		unset($_SESSION['dataokePages'][$flag]);
		
		//exit;

		if(empty($_SESSION['dataokePages']))
		{
			//echo "<br><br>这个显示处理就更新完毕 Session 也清空了";
			$this->success('所有文档更新完毕，页面跳转中...','./productSyn',5);	
			
		}
		else
		{
			//$this->redirect('./Admins/DataokeApi/startSyn', '', 5, '<br><br>页面跳转中...');
			$this->success($flag.'-->玩命更新，数据处理中...','./startSyn',5);
			
		}




		
		
		
	}
	public function delDataokeData()
	{//维护大淘客数据库数据 ：就是删除今天之外的所有数据
	
	}
	public function urlTwoInOne()
	{//二合一链接的拼装
	
	
	}

	public function zhushouApiList()
	{//只在数据库保存4个字段 groupId  goodsId taokeShortUrl time
		$zhoushouApiUrl = "http://api.dataoke.com/index.php?r=goodsLink/qq&type=qq_quan&appkey=wk5awl51op&v=2&page=1";
		$getJson = file_get_contents($zhoushouApiUrl);
		$data = json_decode($getJson,TRUE);
		
		$res_data = $data['data']['result'];
		//var_dump($res_data);
		if($res_data<>NULL)
		{
			foreach ($res_data as $k=>$v)
			{
				$zhoushouData['goodsid'] = $v['GoodsID'];
				$zhoushouData['title'] =  $v['Title'];
				$zhoushouData['ali_click'] =  $v['ali_click'];
				$zhoushouData['promotertype'] = "1";//先默认1 以后扩展
				$zhoushouData['time'] = date('Y-m-d H:i:s');

				$DataokeApi = new \Admins\Model\DataokeApiModel();
				$sqlStr .= $DataokeApi->getZhoushouInsertSql($zhoushouData);
				$dataList[$k] = $zhoushouData;
			}
			//echo $sqlStr;

		}
		//查询兼职列表
		
		$Promoter = new \Admins\Model\PromoterModel();
		$promoterList = $Promoter->getAllList();
		
		$this->assign("promoter",$promoterList);
		$this->assign("dataList",$dataList);
		$this->display();		
	}
	public function insertZhoushouData()
	{

		if(!isset($_POST['promoter'])&&$_POST['promoter']=='')
		{
			exit('页面参数有问题');
		}
		$groupId = $_POST['promoter'];
		if($groupId=="0")
		{
			exit('没有选择推广者');
		}
		$sql = '';
		
		$zhoushouApiUrl = "http://api.dataoke.com/index.php?r=goodsLink/qq&type=qq_quan&appkey=wk5awl51op&v=2&page=1";
		$getJson = file_get_contents($zhoushouApiUrl);
		$data = json_decode($getJson,TRUE);
		$res_data = $data['data']['result'];

		$PHPExcel = new \Admins\Model\PHPExcelModel();
		foreach($res_data as $k=>$v)
		{
			
			$data['groupId'] = $groupId;
			$data['goodsId'] = $v['GoodsID'];
			$data['taokeShortUrl'] = $v['ali_click'];
			$data['time'] = date('Y-m-d H:i:s');
			
			$isNewdata = $PHPExcel->onlyCheck($data['goodsId'],$data['groupId']);
			
			if($isNewdata)
			{//更新操作语句
				$v['id'] = $isNewdata['id'];
				$sql .= $PHPExcel->getZhoushouUpdateSql($data).';';
			}
			else
			{//插入操作语句
				
				$sql .= $PHPExcel->getZhoushouInsertSql($data).';';
				
				
			}		
		}
		//echo $sql;
		
		//定义一个数组，把这些sql语句以一个引用（指针）传过去，不然直接把字符串当参数太大，太消耗内存了。
		$arraySql[] = $sql;
		//echo $arraySql;
		$result = $PHPExcel->moreExecute($arraySql);
		echo "成功！！";
		return $result;	
	}
	public function daterepairView()
	{//数据维护显示
		$this->display();
	}
	public function dataRepair()
	{//数据维护
		

		$DataokeApi = new \Admins\Model\DataokeApiModel();
		$res = $DataokeApi->getTimeProduct();
		$num = $DataokeApi->getTimeProductNum();
		//var_dump($res);
		if($res)
		{
			foreach($res as $k=>$v)
			{
				$r = $DataokeApi->delTimeProduct($v['id']);
				//var_dump($r);
				//深度清理--连推广者的产品也删掉
				//$Promoter = new \Admins\Model\PromoterModel();
				//$Promoter->delPromoterProduct($v['goodsid']);
			}
			$this->redirect('./Admins/DataokeApi/dataRepair','' , 1, "剩余数据$num,维护下一个页面，跳转中...<br/><br/><br/><br/>");
		}
		else
		{
			echo "已经没有数据要维护了...<br/><br/><br/><br/>";
			return false;
		}

	}
	public function quanCheck()
	{
		$nullQuanNum='0';
		$DataokeApi = new \Admins\Model\DataokeApiModel();
		$nullQuanNum = $DataokeApi->getNullQunproductNum();
		//var_dump($nullQuanNum );
		$this->assign("nullQuanNum",$nullQuanNum);
		$this->display();
	}
	public function startQunCheck()
	{//流程是 在总库查到无券的产品后 删除这个产品，同时删除 推广者中 所有等于券的数据
//还没有真实写出来
		$DataokeApi = new \Admins\Model\DataokeApiModel();
		$res = $DataokeApi->getNoquanProduc();
		$num = $DataokeApi->getNoquanProductNum();
		//var_dump($res);
		if($res)
		{
			foreach($res as $k=>$v)
			{

				
				$isQuan = check_Quan($v['sellerid'],$v['quan_id']);
				if($isQuan)
				{
					$info = '可用';
					$value = 2;
					$r = $DataokeApi->updateNoquanProduct($v['id'],$value);
				}
				else
				{
					$info = '过期';
					$value = 1;
					$r = $DataokeApi->updateNoquanProduct($v['id'],$value);
					//var_dump($r);
					
					//深度清理--连推广者的产品也删掉
					$Promoter = new \Admins\Model\PromoterModel();
					$Promoter->delPromoterProduct($v['goodsid']);
					//删除无券的短sclick链接
					$Product_Admins = new \Admins\Model\ProductModel();
					$Product_Admins->delNoQuanSclick($v['goodsid']);					
														
				}


				
			}
			$this->redirect('./Admins/DataokeApi/startQunCheck','' , 1, "剩余数据$num,维护下一个页面，跳转中...<br/><br/><br/><br/>");
		}
		else
		{
			echo "已经没有数据要维护了...<br/><br/><br/><br/>";
			//初始化数据，所有IsQunNull归0，为下一次检测做准备
			
			$DataokeApi->updateInitNoquanProduct();
			return false;
		}

		
	}
	public function delNullQunProduct()
	{//删除已经标注IsQunNull为1的无券数据
		$DataokeApi = new \Admins\Model\DataokeApiModel();
		$r =  $DataokeApi->delNoquanProduct();
		if($r)
		{
			echo "无券数据清空完毕...<br/><br/><br/><br/>";
		}
		else
		{
			echo "无券数据清空失败...<br/><br/><br/><br/>";
		}
		

	}	
	






}

?>