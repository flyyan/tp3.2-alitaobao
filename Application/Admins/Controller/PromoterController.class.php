<?php
namespace Admins\Controller;
use Common\Controller\BaseController;
class PromoterController extends BaseController {

	public function index()
	{
	
	}
	public function oneUpload()
	{
		echo c("XLSFIELD.goodsId");
	
		$this->display();
	}	
	public function PromoterLists()
	{//兼职显示列表
		$Promoter = new \Admins\Model\PromoterModel();
		$result = $Promoter->getList();
		$list = $result['list'];
		$page = $result['page'];
		$this->assign('list',$list);// 赋值数据集
		$this->assign('page',$page);// 赋值分页输出
		
		$this->display();
	}
	public function AddPromoterForm()
	{//添加兼职表单

		
		$this->display();
	}
	public function AddPromoter()
	{//添加兼职逻辑过程
		$Promoter = new \Admins\Model\PromoterModel();

		if(!$Promoter->create())
		{//失败
			exit($Promoter->getError());
		}
		else
		{//写入成功的提示或跳转
		
			$data["groupId"] = $_POST['groupId'];
			$data["groupTitle"] = $_POST['groupTitle'];
			$data["groupName"] = $_POST['groupName'];
			$data["PID"] = $_POST['PID'];
			$data["groupQQ"] = $_POST['groupQQ'];
			$data["isAction"] = ($_POST['isAction']=="on"?"1":"0");
			$data["joinTime"] =  date('Y-m-d h:i:s');

			$res = $Promoter->addPromoter($data);
			if($res)
			{
				$this->success('操作成功');
			}
			else
			{
				$this->error('操作失败');
			}
			
		}		
		
		
	}	
	public function EditPromoterForm($promoterId)
	{//编辑兼职表单

		if(isset($_GET['promoterId']) && $_GET['promoterId']<>'')
		{
			$id = $_GET['promoterId'];
			$Promoter = new \Admins\Model\PromoterModel();
			$res = $Promoter->getPromoter($id);
			if($res)
			{
				$this->assign('promoter',$res[0]);
			}
			
		
		}
		
		$this->display();
	}
	
	public function EditPromoter()
	{//编辑兼职表单
	//var_dump($_POST);
		$res = false;
		if(isset($_POST['id']) && $_POST['id']<>'')
		{
			$data["id"] = $_POST['id'];
			$data["groupId"] = $_POST['groupId'];
			$data["groupTitle"] = $_POST['groupTitle'];
			$data["groupName"] = $_POST['groupName'];
			$data["PID"] = $_POST['PID'];
			$data["groupQQ"] = $_POST['groupQQ'];
			$data["isAction"] = ($_POST['isAction']=="on"?"1":"0");
			$data["joinTime"] =  date('Y-m-d h:i:s');
			$Promoter = new \Admins\Model\PromoterModel();
			$res = $Promoter->editPromoter($data); 
		
		}
			if($res)
			{
				$this->success('操作成功');
			}
			else
			{
				$this->error('操作失败');
			}
	
	}
	public function delPromoter($id)
	{
		if(isset($_GET['id']) && $_GET['id']<>'')
		{
			$promoterid = $_GET['id'];
			$Promoter = new \Admins\Model\PromoterModel();
			//通过$promoterid 查找 $groupid
			$groupId = $Promoter->getGroupId($promoterid);

			//删除推广者所有产品
			$Product = new \Admins\Model\ProductModel();
			$res = $Product->delPromoterProduct($groupId);
			
			//删除推广者
			$Promoter = new \Admins\Model\PromoterModel();
			$res = $Promoter->delPromoter($promoterid);

		}
		if($res)
		{
			$this->success('操作成功');
		}
		else
		{
			$this->error('操作失败');
		}		
		
		

	}	
		
	public function moreUpload()
	{
	
	
		$this->display();
	}
	public function ProductLists()
	{
		
		
		
		$Promoter = new \Admins\Model\PromoterModel();
		$res =  $Promoter->getProductList("女士");
		$list = $res['list'];
		$page = $res['page'];
		$this->assign('list',$list);// 赋值数据集
		$this->assign('page',$page);// 赋值分页输出
		//var_dump($list);
		
		$this->display();
	}	
	public function dataSyn()
	{//数据维护 页面显示页
		$this->display();
	}
	public function fansbuyShortUrlSyn()
	{//拼接链接短网址数据通同步
		//死循环，直到把数据库适合条件的更改完为止
		$Promoter = new \Admins\Model\PromoterModel();
		$res = $Promoter->getFansbuyShortUrlNullList();
		$num = $Promoter->getFansbuyShortUrlNullNum();//需要维护的总数据量
		//var_dump($res);
		if($res)
		{
			foreach($res as  $k => $v)
			{
				if($v['fansbuyurl']<>'')
				{					
					$fansbuyShortUrl = get_shortUrl($v['fansbuyurl']);
					$Promoter->updateFansbuyShortUrl($v['id'],$fansbuyShortUrl);
				}
			}
			$this->redirect('./Admins/Promoter/fansbuyShortUrlSyn','' , 1, "剩余数据量：$num,维护下一个页面，跳转中...");
		}
		else
		{
			echo "已经没有数据要维护了...";
			return false;
		}
	}
	public function fansKonglingSyn()
	{//拼接链接的淘口令数据同步
		$Promoter = new \Admins\Model\PromoterModel();
		$res = $Promoter->getFansKonglingNullList();
		$num = $Promoter->getFansKonglingNullNum();//需要维护的总数据量
		//var_dump($res);
		if($res)
		{
			foreach($res as  $k => $v)
			{
				if($v['fansbuyurl']<>'')
				{					
					$fansKongling = get_fansKouling($v['title'],$v['pic'],$v['fansbuyurl']);
					$Promoter->updateFansKongling($v['id'],$fansKongling);
				}
			}
			$this->redirect('./Admins/Promoter/fansKonglingSyn','' , 1, "剩余数据量：$num,维护下一个页面，跳转中...");
		}
		else
		{
			echo "已经没有数据要维护了...";
			return false;
		}
		
		
	}
	public function uploadify()
	{
		/*
		Uploadify
		Copyright (c) 2012 Reactive Apps, Ronnie Garcia
		Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
		*/		
		// Define a destination
		file_put_contents("log1.txt", "rtrt");
		$targetFolder = '/Uploads'; // Relative to the root
		$verifyToken = md5('unique_salt' . $_POST['timestamp']);			
		if (!empty($_FILES) && $_POST['token'] == $verifyToken)
		{
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
			$targetFile = rtrim($targetPath,'/') . '/' . iconv("UTF-8","GBK",$_FILES['Filedata']['name']);
			
			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif','png','xls'); // File extensions
			//$fileTypes = array('xls');
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			
			if (in_array($fileParts['extension'],$fileTypes))
			{
				//file_put_contents("log1.txt", $tempFile);
				move_uploaded_file($tempFile,$targetFile);
			}
			else
			{
				echo 'Invalid file type.';
			}
		}
	
	
	}

	public function upload(){
		$upload = new \Think\Upload();// 实例化上传类
		$upload->maxSize   =     3145728 ;// 设置附件上传大小
		$upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		$upload->savePath  =      '/Uploads'; // 设置附件上传目录
		// 上传文件 
		$info   =   $upload->upload();
		if(!$info) {// 上传错误提示错误信息
			$this->error($upload->getError());
		}else{// 上传成功
			$this->success('上传成功！');
		}
	}
	public function fileList()
	{
	
		$dir = "./Uploads";
		$arrFile = getFile($dir);
		//var_dump($arrFile);
		foreach ($arrFile as $key=>$val)
		{
			
			if(get_extension($val)=="xls")
			{
				$xlsfiles[$key]['file'] = $val;
				$xlsfiles[$key]['qun'] = substr($val,0,strlen($val)-4);
			}
		}
		
		$this->assign('xlsfiles',$xlsfiles);
		$this->display();
	}
	public function xlsSyn()
	{//xls数据同步
		//var_dump($_POST);
		
		if(!empty($_POST))
		{
			$arr_str = serialize($_POST); 
			unset($_SESSION['xlsFileList']);		
			foreach($_POST as $key=>$val)
			{
				$_SESSION['xlsFileList'][$key] = $val;
			}
			$this->redirect('./Admins/Promoter/startSyn','' , 1, '页面跳转中...');


		}
	}
	public function startSyn()
	{
		foreach($_SESSION['xlsFileList'] as $k=>$v)
		{
			echo  "<b>【".$v."】</b><-----";
		}
		
		$msg = "";
		$flag = "";
		if(empty($_SESSION['xlsFileList']))
		{
			$this->error('操作有误！');
			exit;
		}
		
		foreach($_SESSION['xlsFileList'] as $k=>$v)
		{
			if($v<>NULL)
			{			
				$groupid = get_basename($v);
				$Promoter = new \Admins\Model\PromoterModel();
				$isPromoter = $Promoter->isPromoter($groupid);
				if($isPromoter)
				{
				
					$PHPExcel = new \Admins\Model\PHPExcelModel();
					$res = $PHPExcel->insertData($v,$groupid);
					$flag = $k;	
				
					echo "<br/><br />".$v."处理中";
					for ($i = 0; $i < 3; $i++) {	
					  	flush();
					 	 ob_flush();
					  	sleep(1);				
						ob_end_flush();	
						echo "---";					
					}			
						
						
				}
				else
				{//echo "不可以更新--直接删除session";
					unset($_SESSION['xlsFileList'][$k]);
				}
				@unlink ("./Uploads/".$v); 
				break;
			}
		}
		//$_SESSION['xlsFileList'][$flag] = NULL;

		unset($_SESSION['xlsFileList'][$flag]);
		if(empty($_SESSION['xlsFileList']))
		{//echo "<br><br>这个显示处理就更新完毕 Session 也清空了";
			$msg = "所有文件都已更新完成";	
			$this->success('所有文档更新完毕，页面跳转中...','./moreUpload',5);		
		}
		else
		{
			
			$this->success($v.'更新完毕，页面跳转中...','./startSyn',3);			
		}
		//$this->assign('msg',$msg);
		//$this->display();
	}
	public function delFile($file)
	{
		@unlink ("./Uploads/".$file); 
		$this->redirect('./Admins/Promoter/fileList/','' , 1, '已操作,在跳转...');
	}
	public function delSclick()
	{//删除2天前生产的sclick短连接
		$Product = new \Admins\Model\ProductModel();
		$res = $Product->repairSlick();
		if($res)
		{
			$this->success('操作成功');
		}
		else
		{
			$this->error('操作失败');
		}		
	}
	
	public function delAllSclick()
	{//删除已经生成的所有sclick短连接
		$Product = new \Admins\Model\ProductModel();
		$res = $Product->delAllSclick();
		if($res)
		{
			$this->success('操作成功');
		}
		else
		{
			$this->error('操作失败');
		}		
	}	
	
	


















}



?>