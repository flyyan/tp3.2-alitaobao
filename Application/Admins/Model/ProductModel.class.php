<?php
namespace Admins\Model;
use Think\Model;
class ProductModel extends Model {	
	protected $tableName = 'promoter_dataoke_product';
	
	
	
	public function delPromoterProduct($groupId)
	{//删除指定推广者的产品
	
		$res = $this->where('groupId='.$groupId)->delete(); 
		if($res)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function saveSclick($data)
	{//保存s.click.com生产的短连接
	//数据格式
		$sql = "insert into ".C('DB_PREFIX')."fansbuy_sclick (groupId,goodsId,quanId,sclick,cdate)value('".$data['groupId']."','".$data['goodsId']."','".$data['quanId']."','".$data['sclick']."','".$data['cdate']."')";
		//$data = $this->query($sql);	
		$data = $this->execute($sql);	
		if($data)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public  function  getSclick($goodsid,$groupid)
	{//获取s.click.com生产的短连接
		$sql = "select id,sclick from ".C('DB_PREFIX')."fansbuy_sclick where goodsId = '".$goodsid."' and groupId = '".$groupid."'";
		$data = $this->query($sql);	
		//$data = $this->execute($sql);	
		if($data)
		{
			return true;
		}
		else
		{
			return false;
		}		
	}
	public function repairSlick()
	{//维护生产的短连接
	//这个表将来可能会很大，所以要及时维护
	//删除无券商品和商品维护的时候也要删除这个表
	//删除2天前的数据
		$sql =  "delete FROM ".C('DB_PREFIX')."fansbuy_sclick where cdate < '".date("Y-m-d H:i:s",strtotime("-2 day"))."'";
		
		$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}	
	
	}
	public function delSclick($goodsid,$groupid)
	{//删除数据
		$sql =  "delete FROM ".C('DB_PREFIX')."fansbuy_sclick where goodsId='".$goodsid."' and groupId = '".$groupid."'";
		
		$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}	
	
	}
	public function delNoQuanSclick($goodsid)
	{//删除无券产品是调用的方法
		$sql =  "delete FROM ".C('DB_PREFIX')."fansbuy_sclick where goodsId='".$goodsid."'";
		
		$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}	
	}
	public function delAllSclick()
	{//删除全部的数据
		$sql =  "delete FROM ".C('DB_PREFIX')."fansbuy_sclick where 1";
		
		$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}	
	}

	
	
	
	
	
	
	
}

?>