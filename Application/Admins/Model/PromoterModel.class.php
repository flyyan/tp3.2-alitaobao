<?php
namespace Admins\Model;
use Think\Model;
class PromoterModel extends Model {	
	protected $tableName = 'promoter';
	
	protected $_validate = array(
		//array(验证字段,验证规则,错误提示,[验证条件,附加规则,验证时间])
/*		array('email','require','email没有填写'),
		array('email','email','邮箱格式不对！'),
		array('email','','帐号名称已经存在！',0,'unique',1),
		array('password','require','密码不能为空！'), 
		array('password','password2','确认密码不正确',0,'confirm'),
		array('verify','require','验证码不能为空！'), 
		array('verify','ChVerifyCode','验证码不正确！',1,'callback'),
*/
		
		array('groupId','require','兼职群号没有填写'),
		array('groupId','number','兼职群号必须是数字'),
		array('groupId','','兼职群号已经存在！',0,'unique',1),
		array('groupTitle','require','群名称没有填写'),
		array('groupName ','require','管理姓名没有填写'),
		array('PID','require','PID没有填写'),
		array('groupQQ','require','联系方式没有填写'),
		array('groupId','require','兼职群号没有填写'),
		array('groupId','require','兼职群号没有填写')
	);	
	
	
	public function addPromoter($data)
	{
		$result = $this->data($data)->add();
		return $result;
	
	}
	public function getAllList()
	{
		$list = $this->where()->order('id')->select();
		return $list;
	}	
	public function getList()
	{
		$map = array();//查询条件

		
		import('ORG.Util.Page');// 导入分页类
		$count      = $this->where($map)->count();// 查询满足要求的总记录数
		$Page       = new \Think\Page($count,15);// 实例化分页类 传入总记录数和每页显示的记录数
		//分页跳转的时候保证查询条件
		
		$now = "共：%TOTAL_ROW%条数据 / 共%TOTAL_PAGE%页";
		$total = "%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%";
		$page = "";
		$str = $now." ".$total." ".$page;
		$Page->setConfig('theme',$str);
		foreach($map as $key=>$val) {
			$Page->parameter   .=   "$key=".urlencode($val).'&';
		}

		$page       = $Page->show();// 分页显示输出
		// 进行分页数据查询 注意limit方法的参数要使用Page类的属性
		$list = $this->where($map)->order('id')->limit($Page->firstRow.','.$Page->listRows)->select();
		$result['list'] = $list ;
		$result['page'] = $page;
		
		return $result;
	}
	
	public function isPromoter($groupid)
	{//有无此兼职
		$sql = "select id from ".C('DB_PREFIX')."promoter where groupId = '".$groupid."'";
		$data = $this->query($sql);	
		//$data = $this->execute($sql);	
		if($data)
		{
			return true;
		}
		else
		{
			return false;
		}		
	
	}
	public function getPromoter($id)
	{//有无此兼职
		$sql = "select id,groupId,groupTitle,groupName,PID,groupQQ,isAction from ".C('DB_PREFIX')."promoter where id = '".$id."'";
		$data = $this->query($sql);	
		//$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}		
	
	}
	public function editPromoter($data)
	{
		$result = $this->where('id='.$data['id'])->save($data); 
		return $result;
	}
	public function getGroupId($promoterid)
	{//通过$promoterid 查找 $groupid
		$groupId = $this->where('id='.$promoterid)->getField('groupId');
		if($groupId)
		{
			return $groupId;
		}
		else
		{
			return false;
		}
	}
	public function delPromoter($promoterid)
	{//删除推广者
		$res = $this->where('id='.$promoterid)->delete();
		if($res)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function getOneData($groupId)
	{
		$sql = "select id,groupId,groupTitle,groupName,PID,groupQQ,isAction from ".C('DB_PREFIX')."promoter where groupId = '".$groupId."'";
		$data = $this->query($sql);	
		//$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}		
	}
	
	public function getProductList($keywords = "")
	{
		$where = "1 ";
		if($keywords<>"")
		{
			$where .= " and A.Title Like '%".$keywords."%' ";
		}

		import('ORG.Util.Page');// 导入分页类
		
		$sql1 = "SELECT A.id
FROM  `think_dataoke_product` AS A
INNER JOIN  `think_promoter_dataoke_product` AS B ON A.GoodsID = B.goodsId
WHERE " .$where;
		$count = $this->execute($sql1);

		$Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数
		//分页跳转的时候保证查询条件
		foreach($map as $key=>$val) {
			//$Page->parameter   .=   "$key=".urlencode($val).'&';
		}
		
		$sql = "SELECT A.GoodsID, A.Title, A.D_title, A.pic, A.Org_Price, A.Price, A.Commission, A.Introduce, A.Quan_price, A.Quan_time, A.Quan_condition, A.Quan_link, B.taokeShortUrl, B.quanUrl, B.quanKouling
FROM  `think_dataoke_product` AS A
INNER JOIN  `think_promoter_dataoke_product` AS B ON A.GoodsID = B.goodsId
WHERE ".$where." 
LIMIT ".$Page->firstRow.','.$Page->listRows;
		//echo $sql;
//INNER JOIN 		
		$page       = $Page->show();// 分页显示输出
		$list = $this->query($sql);
		$result['list'] = $list ;
		$result['page'] = $page; 
		return $result;		
		
	}
	public function getFansbuyShortUrlNullList()
	{//获取fansbuyShortUrl为空的数据--值获取30-50条
		$sql = "select id,fansbuyUrl,fansbuyShortUrl,fansKongling from ".C('DB_PREFIX')."promoter_dataoke_product where fansbuyShortUrl='' and fansbuyUrl<>'' Limit 1,50";
		$data = $this->query($sql);	
		//$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}		
	}
	public function getFansbuyShortUrlNullNum()
	{//获取fansbuyShortUrl为空的数据--总记录数
		$sql = "select id from ".C('DB_PREFIX')."promoter_dataoke_product where fansbuyShortUrl='' and fansbuyUrl<>''";
		//$data = $this->query($sql);	
		$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}		
	}
	public function updateFansbuyShortUrl($id,$fansbuyShortUrl)
	{//更新短网址
		$sql = "UPDATE ".C('DB_PREFIX')."promoter_dataoke_product SET `fansbuyShortUrl` = '$fansbuyShortUrl' where `id`=".$id;
		//$data = $this->query($sql);	
		$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}	
	}
	public function getFansKonglingNullList()
	{//获取fansKongling为空的数据--值获取30-50条
		$sql = "select A.id,A.fansbuyUrl,A.fansbuyShortUrl,A.fansKongling,B.Title,B.Pic
		from  `".C('DB_PREFIX')."promoter_dataoke_product` AS A  
		INNER JOIN  `".C('DB_PREFIX')."dataoke_product` AS B ON A.GoodsID = B.goodsId
		where A.fansKongling='' and A.fansbuyUrl<>''  Limit 1,50";
		$data = $this->query($sql);	
		//$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}	
	}
	public function getFansKonglingNullNum()
	{//获取fansKongling为空的数据--总记录数
		$sql = "select A.id
		from  `".C('DB_PREFIX')."promoter_dataoke_product` AS A  
		INNER JOIN  `".C('DB_PREFIX')."dataoke_product` AS B ON A.GoodsID = B.goodsId
		where A.fansKongling='' and A.fansbuyUrl<>''";
		//$data = $this->query($sql);	
		$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}	
	}
	public function updateFansKongling($id,$fansKongling)
	{//更新淘口令
		$sql = "UPDATE ".C('DB_PREFIX')."promoter_dataoke_product SET `fansKongling` = '$fansKongling' where `id`=".$id;
		//$data = $this->query($sql);	
		$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}	
	}	
	public function delPromoterProduct($goodsid)
	{//删除推广者
		$sql =  "delete FROM ".C('DB_PREFIX')."promoter_dataoke_product where goodsId = '".$goodsid."'";
		
		$data = $this->execute($sql);	
		if($data)
		{
			return $data;
		}
		else
		{
			return false;
		}
	}	
	
	
}